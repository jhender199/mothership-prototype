﻿function getemployeejobs() {
    jQuery(document).ready(function ($) {
        $('#holddataclientblock').empty();
        $("#clientblock").show();
        var username = getCookiebyname('uname');
        console.log('test ' + username)
        var jsonstring = "https://wv.mower.com/ws/service.svc/Getalljobs/" + username + "?callback=?";
        $('#thetitleclientblock').empty();
        $('#thetitleclientblock').append('<h3>My Jobs</h3>');
        if (typeof document.getElementById('searchtext') != 'undefined' && document.getElementById('searchtext').value != '') {
            jsonstring = "https://wv.mower.com/ws/service.svc/getsearchjobs/" + document.getElementById('searchtext').value + "?callback=?";
            $('#thetitleclientblock').empty();
            $('#thetitleclientblock').append('<h3>Search Results</h3>');
        }
        buildtable = '<table id="joblist" class="display responsive nowrap" cellspacing="0" width="100%" ><thead><tr><td><b>Actions</b></td><td><b>Job Number</b></td><td><b>Client Code</b></td><td><b>Client Name</b></td><td><b>Job Name</b></td></tr></thead><tbody>'
        $.getJSON(jsonstring, function (data) {
            $.each(data, function (idx, obj) {
                $.each(obj, function (key, value) {
                    var functionstring = '<a href="#" onclick="qva(' + value.JobNumber + ')">QvA</a> '
                    functionstring += '|<a href="#" onclick="alltime(' + value.JobNumber + ')">All</a> '
                    functionstring += '|<a href="#" onclick="functime(' + value.JobNumber + ')">Func</a> '
                    functionstring += '|<a href="#" onclick="mytime(' + value.JobNumber + ')">My</a> '
                    buildtable += '<tr><td>' + functionstring + '</td><td>' + value.JobNumber + '</td><td>' + value.ClientCode + '</td><td>' + value.ClientName + '</td><td>' + value.JobName + '</td></tr>'
                });
            });
            buildtable += '</tbody></table>';
            emptyit();
            $('#holddataclientblock').append(buildtable);
            $('#holddata').append('<table id="jobdata"  class="display responsive nowrap" cellspacing="0" width="100%"><thead><tr><td>Name</td><td>Func</td><td>Hours</td><td>Est Hrs</td><td>Est Amt</td><td>Act Hrs</td><td>Act Amt</td><td>Date</td><td>Comments</td></tr></thead><tfoot><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tfoot><tbody></tbody></table>');
            $('#holddata').hide();
            $('table.display').DataTable({
                responsive: true
            });
        })
        .fail(function (jqXHR, textStatus, errorThrown) { alert('getJSON request failed! ' + errorThrown); })
    });
}
function qva(jobnumber) {
    $('#holddata').show();
    $('#jobdata').DataTable().column(0).visible(false);
    $('#jobdata').DataTable().column(1).visible(true);
    $('#jobdata').DataTable().column(2).visible(false);
    $('#jobdata').DataTable().column(3).visible(true);
    $('#jobdata').DataTable().column(4).visible(true);
    $('#jobdata').DataTable().column(5).visible(true);
    $('#jobdata').DataTable().column(6).visible(true);
    $('#jobdata').DataTable().column(7).visible(false);
    $('#jobdata').DataTable().column(8).visible(false);
    $('#jobdata').DataTable().clear().draw();
    $.getJSON("https://wv.mower.com/ws/service.svc/getqva/" + jobnumber + "?callback=?", function (data) {
        $.each(data, function (idx, obj) {
            $.each(obj, function (key, value) {
                $('#jobdata').DataTable().row.add(['', value.FunctionCode, '', value.EstimatedHours, value.EstimatedAmount, value.EmpHours, value.ActualAmount,  '', '']).draw(false);
            });
        });
        $($('#jobdata').DataTable().column(6).footer()).html($('#jobdata').DataTable().column(6).data().reduce(function (a, b) { return parseFloat(a) + parseFloat(b); }));
        $($('#jobdata').DataTable().column(5).footer()).html($('#jobdata').DataTable().column(5).data().reduce(function (a, b) { return parseFloat(a) + parseFloat(b); }));
        $($('#jobdata').DataTable().column(4).footer()).html($('#jobdata').DataTable().column(4).data().reduce(function (a, b) { return parseFloat(a) + parseFloat(b); }));
        $($('#jobdata').DataTable().column(3).footer()).html($('#jobdata').DataTable().column(3).data().reduce(function (a, b) { return parseFloat(a) + parseFloat(b); }));
    })
        .fail(function (jqXHR, textStatus, errorThrown) { alert('getJSON request failed! ' + errorThrown); });
}
function alltime(jobnumber) {
    $('#holddata').show();
    $('#jobdata').DataTable().column(0).visible(true);
    $('#jobdata').DataTable().column(1).visible(true);
    $('#jobdata').DataTable().column(2).visible(true);
    $('#jobdata').DataTable().column(3).visible(false);
    $('#jobdata').DataTable().column(4).visible(false);
    $('#jobdata').DataTable().column(5).visible(false);
    $('#jobdata').DataTable().column(6).visible(false);
    $('#jobdata').DataTable().column(7).visible(true);
    $('#jobdata').DataTable().column(8).visible(true);
    $('#jobdata').DataTable().clear().draw();
    $.getJSON("https://wv.mower.com/ws/service.svc/getalltime/" + jobnumber + "?callback=?", function (data) {
        $.each(data, function (idx, obj) {
            $.each(obj, function (key, value) {
                if (value.EmpHours != 0.00) {
                    $('#jobdata').DataTable().row.add([value.WholeName, value.FunctionCode, value.EmpHours, '', '', '', '', value.EmpDate, value.EmpComment]).draw(false);
                }
            });
        });
        $($('#jobdata').DataTable().column(2).footer()).html($('#jobdata').DataTable().column(2).data().reduce(function (a, b) { return parseFloat(a) + parseFloat(b); }));
    })
        .fail(function (jqXHR, textStatus, errorThrown) { alert('getJSON request failed! ' + errorThrown); });
}

function mytime(jobnumber) {
    var username = getCookiebyname('uname');
    $('#holddata').show();
    $('#jobdata').DataTable().column(0).visible(false);
    $('#jobdata').DataTable().column(1).visible(true);
    $('#jobdata').DataTable().column(2).visible(true);
    $('#jobdata').DataTable().column(3).visible(false);
    $('#jobdata').DataTable().column(4).visible(false);
    $('#jobdata').DataTable().column(5).visible(false);
    $('#jobdata').DataTable().column(6).visible(false);
    $('#jobdata').DataTable().column(7).visible(true);
    $('#jobdata').DataTable().column(8).visible(true);
    $('#jobdata').DataTable().clear().draw();
    var buildtable = '<table><tr><td colspan="6"><hr></td></tr><tr><td colspan="6"><h3>My Time ' + getCookiebyname('name')+ '</h3></td></tr><tr><td><b>Name</b></td><td><b>Function</b></td><td><b>Hours</b></td><td><b>Date</b></td><td><b>Comments</b></td></tr>';
    $.getJSON("https://wv.mower.com/ws/service.svc/getmytime/" + jobnumber + "/" + username + "?callback=?", function (data) {
        $.each(data, function (idx, obj) {
            $.each(obj, function (key, value) {
                if (value.EmpHours != 0.00) {
                    $('#jobdata').DataTable().row.add(['', value.FunctionCode, value.EmpHours, '', '', '', '', value.EmpDate, value.EmpComment]).draw(false);
                }
                
            });
        });
        $($('#jobdata').DataTable().column(2).footer()).html($('#jobdata').DataTable().column(2).data().reduce(function (a, b) { return parseFloat(a) + parseFloat(b); }));
    })
        .fail(function (jqXHR, textStatus, errorThrown) { alert('getJSON request failed! ' + errorThrown); });
}
function functime(jobnumber) {
    $('#holddata').show();
    $('#jobdata').DataTable().column(0).visible(false);
    $('#jobdata').DataTable().column(1).visible(true);
    $('#jobdata').DataTable().column(2).visible(true);
    $('#jobdata').DataTable().column(3).visible(false);
    $('#jobdata').DataTable().column(4).visible(false);
    $('#jobdata').DataTable().column(5).visible(false);
    $('#jobdata').DataTable().column(6).visible(false);
    $('#jobdata').DataTable().column(7).visible(false);
    $('#jobdata').DataTable().column(8).visible(false);
    $('#jobdata').DataTable().clear().draw();
    $.getJSON("https://wv.mower.com/ws/service.svc/getfunctime/" + jobnumber+ "?callback=?", function (data) {
        $.each(data, function (idx, obj) {
            $.each(obj, function (key, value) {
                if (value.EmpHours != 0.00) {
                    $('#jobdata').DataTable().row.add(['', value.FunctionCode, value.EmpHours, '', '', '', '', '', '']).draw(false);
                }
            });
        });
        $($('#jobdata').DataTable().column(2).footer()).html($('#jobdata').DataTable().column(2).data().reduce(function (a, b) { return parseFloat(a) + parseFloat(b); }));
    })
        .fail(function (jqXHR, textStatus, errorThrown) { alert('getJSON request failed! ' + errorThrown); });
}
