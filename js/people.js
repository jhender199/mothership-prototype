﻿$body = $("body");
var currentUser;
var holdoffices2=[];
var holddepts2=[];

function imgError(image) {
    image.onerror = "";
    image.src = "https://wv.mower.com/ws/images/logo.jpg";
    return true;
}

function doupdate(uname){
    var username = getCookiebyname('uname');
    var myObject = new Object();
    myObject.WholeName= '';
    //myObject.username=username[0] ;
    myObject.username=uname ;
    myObject.BIO_NOW= $('#now').val();
    myObject.BIO_BORN= $('#born').val();
    myObject.BIO_SCHOOL= $('#school').val();
    myObject.BIO_WORKED= $('#worked').val();
    myObject.BIO_SIGNIF= $('#signif').val();
    myObject.BIO_KIDS= $('#kids').val();
    myObject.BIO_PETS= $('#pets').val();
    myObject.BIO_ACT= $('#act').val();
    myObject.BIO_BOOK= $('#book').val();
    myObject.BIO_MOVIE= $('#movie').val();
    myObject.BIO_MUSIC= $('#music').val();
    myObject.BIO_REST= $('#rest').val();
    myObject.BIO_SATURDAY= $('#saturday').val();
    myObject.BIO_TRAVELLED= $('#travelled').val();
    myObject.BIO_LIVE= $('#live').val();
    myObject.BIO_VACATION= $('#vacation').val();
    myObject.BIO_LOTTERY= $('#lottery').val();
    myObject.BIO_ADVICE= $('#advice').val();
    myObject.BIO_TAGLINE= $('#tagline').val();
    myObject.BIO_QUOTE= $('#quote').val();
    myObject.BIO_NOTES= $('#notes').val();
    myObject.BIO_WHAT=$('#whatwhen').val();
    myObject.Website='False';
    if (document.getElementById('onmower').checked){myObject.Website='True';}
    myObject.Leadership='False';
    if (document.getElementById('leadership').checked){myObject.Leadership='True';}
    myObject.Question=$('#question').val();
    myObject.Answer=$('#answer').val();
    myObject.Dept=document.getElementById("dept").options[document.getElementById("dept").selectedIndex].value;
    var mystring =JSON.stringify(myObject) ;
    //alert(mystring );
    //prompt("Copy to clipboard: Ctrl+C, Enter", mystring );
    $.ajax({         
        type: "POST",
        url:"https://wv.mower.com/ws/service.svc/saveperson/",
        data: mystring ,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
        success: function (data, status, jqXHR) {
            //alert("success… " + data);
            domodal2(uname);
        },
        error: function (xhr,textStatus, errorThrown) {
            alert("Error_ " + xhr.responseXML + textStatus,errorThrown);
        }  
    });

}
function domodal2(uname){
    jQuery(document).ready(function($) {
        var username = getCookiebyname('uname');
        var jsonstring = "https://wv.mower.com/ws/service.svc/getperson/" + uname + "?callback=?";
        var holdofficeid=0;
        buildcontent='';
        buildtable ='<table align="left">';
        buildtitle='';
        officetitle='not found';
        $.getJSON(jsonstring, function (data) {
           // alert(JSON.stringify(data['GetPersonResult'][0].Title))
            $.each(data, function(idx, obj){ 
                $.each(obj, function(key, value){	         
                    buildtable +='<tr><td><img src="https://wv.mower.com/ws/images/' +  value['username'] + '600.jpg" onerror="imgError(this);" /><td></tr>';
                    buildtitle=value['WholeName'] + ' - ' + value['Title'] + ' - ' + value['PhoneNumber']+ ' - Boss:' + value['Supervisor'];
                    buildcontent+= '<h3>All about Me</h3><input type="button" value="update" name="Update" onclick="doupdate(\'' + uname + '\');"><br>';
                    buildcontent +='Birthday: <strong>' +value['BirthDay'] +'</strong><br>';
                    buildcontent +='What do you do and when should I call you? <textarea id=whatwhen>' + value['BIO_WHAT'] + '</textarea><br>' ;
                    buildcontent +='Which side of the city, county or state do you live on? <textarea id=now>' + value['BIO_NOW'] + '</textarea><br>' ;
                    buildcontent +='Where were you born (yes, in a hospital, but where?)? <textarea id=born>' + value['BIO_BORN'] + '</textarea><br>' ;
                    buildcontent +='Where did you go to school? <textarea id=school>' + value['BIO_SCHOOL'] + '</textarea><br>' ;
                    buildcontent +='Where have you worked? <textarea id=worked>' + value['BIO_WORKED'] + '</textarea><br>' ;
                    buildcontent +='<h3>All in the Family</h3>';
                    buildcontent +='Name of significant other ... <textarea id=signif>' + value['BIO_SIGNIF'] + '</textarea><br>' ;
                    buildcontent +='Any children? Grandchildren? What are their names, and what year were they born? <textarea id=kids>' + value['BIO_KIDS'] + '</textarea><br>';
                    buildcontent +='Got pets? What are they, and what are their names? <textarea id=pets>' + value['BIO_PETS'] + '</textarea><br>';
                    buildcontent +='<h3>Getting to Know You</h3>';
                    buildcontent +='What kinds of activities and hobbies interest you most? <textarea id=act>' + value['BIO_ACT'] + '</textarea><br>';
                    buildcontent +='Your favorite Book? <textarea id=book>' + value['BIO_BOOK'] + '</textarea><br>';
                    buildcontent +='Your favorite Movie? <textarea id=movie>' + value['BIO_MOVIE'] + '</textarea><br>';
                    buildcontent +='Your favorite Kind of Music? <textarea id=music>' + value['BIO_MUSIC'] + '</textarea><br>';
                    buildcontent +='Your favorite Restaurant? <textarea id=rest>' + value['BIO_REST'] + '</textarea><br>';
                    buildcontent +='It\'s 2 p.m. on Saturday - what are you doing? <textarea id=saturday>' + value['BIO_SATURDAY'] + '</textarea><br>';
                    buildcontent +='What parts of the world have you seen? <textarea id=travelled>' + value['BIO_TRAVELLED'] + '</textarea><br>';
                    buildcontent +='If you could live anywhere else in the world, where would it be? <textarea id=live>' + value['BIO_LIVE'] + '</textarea><br>';
                    buildcontent +='What\'s your dream vacation? <textarea id=vacation>' + value['BIO_VACATION'] + '</textarea><br>';
                    buildcontent +='If you won the Lottery, what would be the first thing you\'d do? <textarea id=lottery>' + value['BIO_LOTTERY'] + '</textarea><br>';
                    buildcontent +='What\'s the best advice you\'ve ever heard? <textarea id=advice>' + value['BIO_ADVICE'] + '</textarea><br>';
                    buildcontent +='If people had taglines, what would yours be? <textarea id=tagline>' + value['BIO_TAGLINE'] + '</textarea><br>';
                    buildcontent +='What\'s your favorite quote? <textarea id=quote>' + value['BIO_QUOTE'] + '</textarea><br>';
                    buildcontent +='Anything you\'d like to add? <textarea id=notes>' + value['BIO_NOTES'] + '</textarea><br>';	
                    if(username.toLowerCase() !='jhenderson' && username.toLowerCase() !='kgray')
                    {
                        buildcontent+='<div style="display:none;">'
                    }
                    buildcontent +='<br/> _____________________ADMIN INFO_________________<br/><br/>';
                    if (value['Website'] == 'True')
                    {
                        buildcontent +='Show on mower.com? <input type="checkbox" id="onmower" checked><br>';
                    }
                    else
                    {
                        buildcontent +='Show on mower.com? <input type="checkbox" id="onmower"><br>';
                    }	
                    if (value['Leadership'] == 'True')
                    {
                        buildcontent +='Leadership? <input type="checkbox" id="leadership" checked><br>';
                    }
                    else
                    {
                        buildcontent +='Leadership? <input type="checkbox" id="leadership"><br>';
                    }	
                    buildcontent +='Title: <textarea id=title>' + value['Title'] + '</textarea><br>';
                    buildcontent +='Mower.com Question <textarea id=question> ' + value['Question'] + '</textarea> Answer: <textarea id=answer>' + value['Answer'] +'</textarea><br/>';
                    buildcontent +='Department<select id="dept">';
                    selected='';
                    if (value['Dept']=='Account Service'){selected='selected';}
                    buildcontent +='<option value="Account Service" ' + selected +'>Account Service</option>';
                    selected='';
                    if (value['Dept']=='Management Services'){selected='selected';}
                    buildcontent +='<option value="Management Services" ' + selected +'>Management Services</option>';
                    selected='';
                    if (value['Dept']=='Digital + Direct'){selected='selected';}
                    buildcontent +='<option value="Digital + Direct" ' + selected +'>Digital + Direct</option>';
                    selected='';
                    if (value['Dept']=='Creative'){selected='selected';}
                    buildcontent +='<option value="Creative" ' + selected +'>Creative</option>';
                    selected='';
                    if (value['Dept']=='Media'){selected='selected';}
                    buildcontent +='<option value="Media" ' + selected +'>Media</option>';
                    selected='';
                    if (value['Dept']=='Public Relations + Public Affairs'){selected='selected';}
                    buildcontent +='<option value="Public Relations + Public Affairs" ' + selected +'>Public Relations + Public Affairs</option>';
                    selected='';
                    if (value['Dept']=='EMA Insight'){selected='selected';}
                    buildcontent +='<option value="EMA Insight" ' + selected +'>EMA Insight</option></select>';
                    if(username.toLowerCase() !='jhenderson')
                    {
                        buildcontent+='</div>'
                    }

                });
            });    
            buildtable +='</table>';
            $("#dialog").empty();
            $("#dialog").append(buildtable);
            $("#dialog").append(buildcontent);
            $("#dialog").dialog({  height: 600,width: 800,title: buildtitle});
        })
        .fail(function(jqXHR, textStatus, errorThrown) { alert('getJSON request failed! ' + errorThrown); })      
    });

}

function domodalmain(uname){
    jQuery(document).ready(function($) {
        var username = getCookiebyname('uname');
        var jsonstring = "https://wv.mower.com/ws/service.svc/getperson/" + uname + "?callback=?";
        var holdofficeid=0;
        buildcontent='';
        buildtable ='<table align="left">';
        buildtitle='';
        officetitle='not found';
        $.getJSON(jsonstring, function( data ) {
            $.each(data, function(idx, obj){ 
                $.each(obj, function(key, value){	         
                    buildtable +='<tr><td><img src="https://wv.mower.com/ws/images/' +  value['username'] + '600.jpg" onerror="imgError(this);" /><td></tr>';
                    buildtitle=value['WholeName'] + ' - ' + value['Title'] + ' - ' + value['PhoneNumber'] + ' - Boss:' + value['Supervisor'];
                    buildcontent+= '<h3>All about Me</h3>';
                    buildcontent +='Birthday: <strong>' +value['BirthDay'] +'</strong><br>';
                    if( value['BIO_WHAT'] !=''){buildcontent +='What do you do and when should I call you? <strong>' + value['BIO_WHAT'] + '</strong><br>' };
                    if( value['BIO_NOW'] !=''){buildcontent +='Which side of the city, county or state do you live on? <strong>' + value['BIO_NOW'] + '</strong><br>' };
                    if( value['BIO_BORN'] !=''){buildcontent +='Where were you born (yes, in a hospital, but where?)? <strong>' + value['BIO_BORN'] + '</strong><br>' };
                    if( value['BIO_SCHOOL'] !=''){buildcontent +='Where did you go to school? <strong>' + value['BIO_SCHOOL'] + '</strong><br>' };
                    if( value['BIO_WORKED'] !=''){buildcontent +='Where have you worked? <strong>' + value['BIO_WORKED'] + '</strong><br>' };
                    buildcontent +='<h3>All in the Family</h3>';
                    if( value['BIO_SIGNIF'] !=''){buildcontent +='Name of significant other ... <strong>' + value['BIO_SIGNIF'] + '</strong><br>' };
                    if( value['BIO_KIDS'] !=''){buildcontent +='Any children? Grandchildren? What are their names, and what year were they born? <strong>' + value['BIO_KIDS'] + '</strong><br>'};
                    if( value['BIO_PETS'] !=''){buildcontent +='Got pets? What are they, and what are their names? <strong>' + value['BIO_PETS'] + '</strong><br>'};
                    buildcontent +='<h3>Getting to Know You</h3>';
                    if( value['BIO_ACT'] !=''){buildcontent +='What kinds of activities and hobbies interest you most? <strong>' + value['BIO_ACT'] + '</strong><br>'};
                    if( value['BIO_BOOK'] !=''){buildcontent +='Your favorite Book? <strong>' + value['BIO_BOOK'] + '</strong><br>'};
                    if( value['BIO_MOVIE'] !=''){buildcontent +='Your favorite Movie? <strong>' + value['BIO_MOVIE'] + '</strong><br>'};
                    if( value['BIO_MUSIC'] !=''){buildcontent +='Your favorite Kind of Music? <strong>' + value['BIO_MUSIC'] + '</strong><br>'};
                    if( value['BIO_REST'] !=''){buildcontent +='Your favorite Restaurant? <strong>' + value['BIO_REST'] + '</strong><br>'};
                    if( value['BIO_SATURDAY'] !=''){buildcontent +='It\'s 2 p.m. on Saturday - what are you doing? <strong>' + value['BIO_SATURDAY'] + '</strong><br>'};
                    if( value['BIO_TRAVELLED'] !=''){buildcontent +='What parts of the world have you seen? <strong>' + value['BIO_TRAVELLED'] + '</strong><br>'};
                    if( value['BIO_LIVE'] !=''){buildcontent +='If you could live anywhere else in the world, where would it be? <strong>' + value['BIO_LIVE'] + '</strong><br>'};
                    if( value['BIO_VACATION'] !=''){buildcontent +='What\'s your dream vacation? <strong>' + value['BIO_VACATION'] + '</strong><br>'};
                    if( value['BIO_LOTTERY'] !=''){buildcontent +='If you won the Lottery, what would be the first thing you\'d do? <strong>' + value['BIO_LOTTERY'] + '</strong><br>'};
                    if( value['BIO_ADVICE'] !=''){buildcontent +='What\'s the best advice you\'ve ever heard? <strong>' + value['BIO_ADVICE'] + '</strong><br>'};
                    if( value['BIO_TAGLINE'] !=''){buildcontent +='If people had taglines, what would yours be? <strong>' + value['BIO_TAGLINE'] + '</strong><br>'};
                    if( value['BIO_QUOTE'] !=''){buildcontent +='What\'s your favorite quote? <strong>' + value['BIO_QUOTE'] + '</strong><br>'};
                    if( value['BIO_NOTES'] !=''){buildcontent +='Anything you\'d like to add? <strong>' + value['BIO_NOTES'] + '</strong><br>'};	
                });
            });    
            buildtable +='</table>';
            $("#dialog").empty();
            $("#dialog").append(buildtable);
            $("#dialog").append(buildcontent);
            $( "#dialog" ).dialog({  height: 600,width: 800,title: buildtitle});
        })
        .fail(function(jqXHR, textStatus, errorThrown) { alert('getJSON request failed! ' + errorThrown); })      
    });

}
function domodal3(uname){
    jQuery(document).ready(function($) {
        var username = getCookiebyname('uname');
        var jsonstring = "https://wv.mower.com/ws/service.svc/getperson/" + uname + "?callback=?";
        var holdofficeid=0;
        buildcontent='';
        buildtable ='<table align="left">';
        buildtitle='';
        officetitle='not found';
        $.getJSON(jsonstring, function( data ) {
            $.each(data, function(idx, obj){ 
                $.each(obj, function(key, value){	         
                    buildtitle=value['WholeName'] + ' - ' + value['Title'] + ' - ' + value['PhoneNumber'] + ' - Boss:' + value['Supervisor'];
                    buildcontent+='<div>';
          	
          	
                    buildcontent+='<span style="font-size: 42px; font-family:Arial, sans-serif; mso-line-height-rule: exactly; color:#F2B33A"><span style="font-size: 42px; color:#F2B33A;"><b>__</b></span><span style="font-size: 12px; color: #ffffff">_</span><span style="font-size: 42px; color:#9BA20B;"><b>__</b></span><span style="font-size:12px; color: #ffffff">_</span><span style="font-size: 42px; color:#5891AC;"><b>_</b></span><span style="font-size: 12px; color: #ffffff">_</span><span style="font-size: 42px; color:#CC445E;"><b>___</b></span><span style="font-size: 12px; color:#ffffff">_</span><span style="font-size: 42px; color:#850028;"><b>__</b></span><span style="font-size: 12px; color: #ffffff">_</span> </span> <span style="font-size: 12px; font-family: Arial, sans-serif; mso-line-height-rule: exactly; color:#C81C30"><strong><br><br><br>';
                    buildcontent+=value['WholeName'] + '</strong><span style="color: #695F57; font-family: Georgia" >&nbsp;&nbsp;|&nbsp;&nbsp;</span><span style="color: #695F57" ><i><font face="Georgia">' + value['Title'] + '</font></i></span><br><br>'
                    buildcontent+='</span> <span style="color: #695F57; font-size: 11px; font-family: Arial, sans-serif; mso-line-height-rule: exactly;"><strong>ERIC MOWER + ASSOCIATES<br>'
                    for (i=0;i<holdoffices2.length;++i) 
                    {
                        if (value['OfficeId'] == holdoffices2[i]['OfficeId'])
                        {
                            var officetitle = holdoffices2[i]['OfficeName']; 
                            var officeaddr = holdoffices2[i]['OfficeAddress'];
                            var officecity = holdoffices2[i]['OfficeCity'];
                            var officestate = holdoffices2[i]['OfficeState'];
                            var officezip = holdoffices2[i]['OfficeZip'];
                            var officephone = holdoffices2[i]['OfficePhone'];

                        }   					
                    }
                    buildcontent+='</strong></span><span style="color: #695F57; font-size: 11px; font-family: Arial, sans-serif; mso-line-height-rule: exactly;">' + officeaddr + ',&nbsp' + officecity + ',&nbsp' + officestate + ' ' + officezip +  '<br>';
                    buildcontent +='</span><span style="color: #695F57; font-size: 11px; font-family: Arial, sans-serif; mso-line-height-rule: exactly;">P 123.444.5555 &nbsp; &nbsp; C 123.444.6666<br>';
                    buildcontent +='</span><span style="color: #695F57; font-size: 11px; font-family: Arial, sans-serif; mso-line-height-rule: exactly;"><a href="http://www.mower.com" target="_blank" style="text-decoration: none; color:#695F57">www.mower.com</a><br><br>'
                    buildcontent +='</span><span style="color: #695F57; font-size: 9px; font-family: Arial, sans-serif; mso-line-height-rule: exactly;">ALBANY &nbsp;&bull;&nbsp; ATLANTA &nbsp;&bull;&nbsp; BOSTON &nbsp;&bull;&nbsp; BUFFALO &nbsp;&bull;&nbsp; CHARLOTTE &nbsp;&bull;&nbsp; CINCINNATI &nbsp;&bull;&nbsp; NEW YORK CITY &nbsp;&bull;&nbsp; ROCHESTER &nbsp;&bull;&nbsp; SYRACUSE<br><br>';
                    buildcontent +='</span> <span style="color: #695F57; font-size: 11px; font-family: Arial, sans-serif; mso-line-height-rule: exactly;"><i><b>2X Advertising Age Top 10 Places to Work</b></i><br><br>';
                    buildcontent +=' </span><div style="display:block; white-space:nowrap; font:15px courier; color:#ffffff; text-align:left" class="mobile-hide"> &nbsp; - &nbsp; - &nbsp; - &nbsp; - &nbsp; - &nbsp; - &nbsp; - &nbsp;  - &nbsp;  - &nbsp;  - &nbsp;  - &nbsp;  - &nbsp;  - &nbsp;  - &nbsp;  -  &nbsp;  - </div>';

                });
            });    

            $("#dialog").empty();

            $("#dialog").append(buildcontent);
            $( "#dialog" ).dialog({  height: 600,width: 800,title: buildtitle});
    
        })
        .fail(function(jqXHR, textStatus, errorThrown) { alert('getJSON request failed! ' + errorThrown); })      
    });

}
function getallpeople(used) {	
    jQuery(document).ready(function ($) {
        $("#peopleblock").show();
        //Get Offices first
        var jsonstring1 = "https://wv.mower.com/ws/service.svc/getoffices?callback=?";
        $.getJSON(jsonstring1, function( data2 ) {
            $.each(data2, function(idx2, obj2){ 
                $.each(obj2, function(key2, value2){
                    holdoffices2.push(value2)
                });
            });
            var username = getCookiebyname('uname');
            var jsonstring = "https://wv.mower.com/ws/service.svc/getpeople?callback=?";
            $('#thetitle2').empty();
            var holdofficeid = 0;
            buildtable = '<table><tr><td></td><td><b>Employee</b></td><td><b>Title</b></td><td><b>Birthday</b></td><td><b>Anniversary</b></td></tr>'
            officetitle = 'not found';
            selectedoffice = document.getElementById("theoffice").options[document.getElementById("theoffice").selectedIndex].value;
            selecteddept = document.getElementById("thedept").options[document.getElementById("thedept").selectedIndex].value;
            $.getJSON(jsonstring, function (data) {
                $.each(data, function (idx, obj) {
                    $.each(obj, function (key, value) {
                        if ((selectedoffice == 'all' && selecteddept == 'all') || (selectedoffice == value['OfficeId'] && selecteddept == 'all') || (selectedoffice == 'all' && selecteddept == value['Dept2']) || (selecteddept == value['Dept2'] && selectedoffice == value['OfficeId'])) {
                            if (value['OfficeId'] != holdofficeid) {

                                for (i = 0; i < holdoffices2.length; ++i) {
                                    if (value['OfficeId'] == holdoffices2[i]['OfficeId']) {
                                        var officetitle = holdoffices2[i]['OfficeName'];
                                        var officeaddr = holdoffices2[i]['OfficeAddress'];
                                        var officecity = holdoffices2[i]['OfficeCity'];
                                        var officestate = holdoffices2[i]['OfficeState'];
                                        var officezip = holdoffices2[i]['OfficeZip'];
                                        var officephone = holdoffices2[i]['OfficePhone'];

                                    }
                                }
                                buildtable += '<tr><td colspan="4"><strong>' + officetitle + ' Office <br>' + officeaddr + '<br>' + officecity + ',' + officestate + ' ' + officezip + '<br>' + officephone + '</strong></td></tr>';
                            }

                            if (username.toLowerCase() == value['username'].toLowerCase() || username.toLowerCase() == 'jhenderson' || username.toLowerCase() == 'kgray') {
                                buildtable += '<tr><td><img src="https://wv.mower.com/ws/images/' + value['username'] + '.jpg" onerror="imgError(this);" /></td><td><a href="#" onClick="domodal2(&quot;' + value['username'] + '&quot;);">Edit: ' + value['WholeName'] + '</a> | <a href="#" onClick="domodal3(&quot;' + value['username'] + '&quot;);">Email Signature</a></td><td>' + value['Title'] + '</td><td>' + value['BirthDay'] + '</td><td>' + value['Anniversary'] + '</td></tr>';
                            }
                            else {
                                buildtable += '<tr><td><img src="https://wv.mower.com/ws/images/' + value['username'] + '.jpg" onerror="imgError(this);" /></td><td><a href="#" onClick="domodalmain(&quot;' + value['username'] + '&quot;);">' + value['WholeName'] + '</a> </td><td>' + value['Title'] + '</td><td>' + value['BirthDay'] + '</td><td>' + value['Anniversary'] + '</td></tr>';
                            }
                            holdofficeid = value['OfficeId'];
                        }
                    });

                });
                buildtable += '</table>';
                emptyit2();
                $('#holddata2').append(buildtable);
            })
            .fail(function (jqXHR, textStatus, errorThrown) { alert('getJSON request failed! ' + errorThrown); })
        });
        
    });
}
function emptyit2()

{
    $('#holddata2').empty();

}


