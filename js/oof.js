﻿function getoof(thedate,type,howmanytimes) {
    var buildtable = '';
    
    jQuery(document).ready(function ($) {
        $("#oofblock").show();	
        var jsonstring = "https://wv.mower.com/ws/service.svc/getoof2/" + thedate + "?callback=?";
       
        var firsttime = 'yes';
        var holdname = '';
        var holdoffice = '';
        if (moment(thedate).format("MM-DD-YYYY") == moment().format( "MM-DD-YYYY"))
        {
            buildtable += '<h3>Today</h3>';
        }
        else
        {
            buildtable += '<h3>' + moment(thedate).format("MM-DD-YYYY") + '</h3>';
        }
        if (type == 'weekly') {
            $("#oofblock2").show();
        }
        else
        {
            $("#officeoof").val('99');
            $("#deptoof").val('99');
            buildtable += '<a href="#" onClick="emptyit2();getoof(\'' + moment(thedate).subtract(1, 'days').format("MM-DD-YYYY") + '\');">Previous Day</a><--><a href="#" onClick="emptyit2();getoof(\'' + moment(thedate).add(1, 'days').format("MM-DD-YYYY") + '\');">Next Day</a> or <a href="#" onclick="emptyit2();weeklygetoof(\'' + moment(thedate).format("MM-DD-YYYY") + '\');"> View by Week</a><br/>';
        }
        
        $.getJSON(jsonstring, function( data ) {
            $.each(data, function (idx, obj) {
                $.each(obj, function (key, value) {
                    
                    if (($('#officeoof').val() == '99' || $('#officeoof').val() == '' || $('#officeoof').val() == value.Office) && ($('#deptoof').val() == '99' || $('#deptoof').val() == '' || $('#deptoof').val() == value.Department)) {
                        if (firsttime == 'yes' || holdoffice != value.Office) {
                            buildtable += '<br/><br/><h3>' + value.Office + '</h3>'
                        }
                        if (holdname != value.FirstName + value.LastName || firsttime == 'yes') {
                            if (firsttime == 'no') buildtable += '<br/>';
                            buildtable += value.FirstName + ' ' + value.LastName + ' ' + moment(value.StartTime).format("hh:mm a") + ' - ' + moment(value.EndTime).format("hh:mm a");
                        }
                        else {
                            buildtable += ', ' + moment(value.StartTime).format("hh:mm a") + ' - ' + moment(value.EndTime).format("hh:mm a");
                        }
                        holdname = value.FirstName + value.LastName;
                        holdoffice = value.Office;
                        firsttime = 'no';
                    }
                });
            });    
            
             $('#holddataoofblock').append(buildtable); 
        })
        .fail(function(jqXHR, textStatus, errorThrown) { alert('getJSON request failed! ' + errorThrown); })      
    });
}
function weeklygetoof(thedate) {
    emptyit2();
    if ($("#weekof").val() !='') thedate =  moment($("#weekof").val()).format("MM-DD-YYYY");
    thedate = getMonday(thedate);
    var whichdate = moment(thedate).format("MM-DD-YYYY");
    for (var i = 0; i < 5; i++) {
        setTimeout(function () {
            getoof(whichdate, 'weekly', i);
            whichdate = moment(whichdate).add(1, 'd').format("MM-DD-YYYY");
        }, i * 250);
       
    }
}
function outputoof()
{
    for (var i = 0; i < 5; i++) {
        $('#holddataoofblock').append(oofdata[i]);
    }
}
function emptyit2()

{   
    $('#holddataoofblock').empty();
    $("#oofblock2").hide();
}



