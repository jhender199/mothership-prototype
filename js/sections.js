﻿//Document Library
function getdocumentlibrary() {
    //Agency Files from SharePoint. Filtered by
    //Add others Accounting 97525365-4430-451F-90CD-4D631A00870, HR C9AEDDF9A02C4CE3B6AE997D69AAA820, IT 160AB46E5A384AFD9FFD328E1AF2CAC0, Agency D9B30730109F49F199BBBFE50705843B
    //Adding video and links will require using the advanced SharePoint interface and adding a link. Then modify to add title and description then use that to determine what happens when you click on it.
    var agencyfilesurl = '';
    //Cache logic needs to go here
    //Run Agency
    //Get Cache 
    var filescached = 'no';
    var cacheddata = [];
    var typeurl = $("#pagebuilder").val();
    if ($("#pagebuilder").val() == 'Public Relations & Public Affairs') typeurl = 'PublicRelationsandPublicAffairs';
    var jsonstring1 = 'https://wv.mower.com/ws/service.svc/getfilecache/' + typeurl.replace(/\s+/g, '') + '?callback=?';
    //console.log('Before Cache Data ');
    $.getJSON(jsonstring1, function (data2) {
        //console.log('Checking Cache Data ' );
        $.each(data2, function (idx2, obj2) {
            $.each(obj2, function (key2, value2) {
                //Add code to see if this is not blank. If it's blank then set fiescached='yes'
                //console.log('what I found ' + value2.CacheData)
                if (value2.CacheData != '' && value2.CacheData != '[]') {
                    filescached = 'yes'
                    console.log('Getting Cache Data ');
                    cacheddata = value2.CacheData.split('|')
                }
                if (filescached == 'yes') {
                    $('#groupfileholder').show();
                    $('#grouptitleholder').html('<strong>' + $("#pagebuilder").val() + ' Files' + '</strong>');;
                    //console.log('Rendering Cached data');
                    for (var i = 0; i < cacheddata.length; i++) {
                        if (cacheddata[i] == '[Information Technology]') {
                            $('#jstree3_div').jstree({
                                'core': { 'data': JSON.parse(cacheddata[i + 1]), check_callback: true, expand_selected_onload: false }
                            }).bind("select_node.jstree", function (e, data) {
                                window.open(data.node.a_attr.href, data.node.a_attr.target);
                            });
                            $("#jstree3_div").jstree("close_all");
                        }
                        if (cacheddata[i] == '[Accounting]') {
                            $('#jstree4_div').jstree({
                                'core': { 'data': JSON.parse(cacheddata[i + 1]), check_callback: true, expand_selected_onload: false }
                            }).bind("select_node.jstree", function (e, data) {
                                window.open(data.node.a_attr.href, data.node.a_attr.target);
                            });
                            $("#jstree4_div").jstree("close_all");
                        }
                        if (cacheddata[i] == '[Human Resources]') {
                            $('#jstree5_div').jstree({
                                'core': { 'data': JSON.parse(cacheddata[i + 1]), check_callback: true, expand_selected_onload: false }
                            }).bind("select_node.jstree", function (e, data) {
                                window.open(data.node.a_attr.href, data.node.a_attr.target);
                            });
                            $("#jstree5_div").jstree("close_all");
                        }
                        if (cacheddata[i] == '[Agency]') {
                            //console.log('Rendering Agency Cached data' + cacheddata[i + 1]);
                            $('#jstree6_div').jstree({
                                'core': { 'data': JSON.parse(cacheddata[i + 1]), check_callback: true, expand_selected_onload:false }
                            }).bind("select_node.jstree", function (e, data) {
                                window.open(data.node.a_attr.href, data.node.a_attr.target);
                            });
                            $("#jstree6_div").jstree("close_all");
                            
                        }
                    }

                }
                else {
                    //Will need to set this to cache all of the data together and compress on the other end
                    cachedataholder = '';
                    getagencyfiles('https://graph.microsoft.com/v1.0/sites/mower.sharepoint.com,7ff72d6f-ed85-4f94-b92b-a34e78322b7a,a43fa114-7179-417a-8a62-6362813d0447/lists/D9B30730-109F-49F1-99BB-BFE50705843B/items?expand=fields&top=1000', $("#pagebuilder").val());
                    //Run IT
                    getagencyfiles('https://graph.microsoft.com/beta/sites/mower.sharepoint.com,7ff72d6f-ed85-4f94-b92b-a34e78322b7a,a43fa114-7179-417a-8a62-6362813d0447/lists/160AB46E-5A38-4AFD-9FFD-328E1AF2CAC0/items?expand=fields', 'Information Technology');
                    //Run Accounting
                    getagencyfiles('https://graph.microsoft.com/beta/sites/mower.sharepoint.com,7ff72d6f-ed85-4f94-b92b-a34e78322b7a,a43fa114-7179-417a-8a62-6362813d0447/lists/97525365-4430-451F-90CD-4D631A00870A/items?expand=fields', 'Accounting');
                    //Run HR
                    getagencyfiles('https://graph.microsoft.com/beta/sites/mower.sharepoint.com,7ff72d6f-ed85-4f94-b92b-a34e78322b7a,a43fa114-7179-417a-8a62-6362813d0447/lists/C9AEDDF9-A02C-4CE3-B6AE-997D69AAA820/items?expand=fields', 'Human Resources');

                }

            });
        });
    });
}
//Get Events
function getevents(eventtypes) {
    //Annoucements
    $.ajax({
        url: "https://graph.microsoft.com/v1.0/sites/mower.sharepoint.com,dc7f3b7e-5123-47ba-bcf1-a961abc250e7,504469bd-9e8d-4ecf-9e6b-f4d6e9743557/lists/0ADBBD66-8339-4625-915C-CC27D81A4D91/items?expand=fields&orderby=Title%20asc",
        headers: { "Authorization": "Bearer " + token2 },
        cache: false
    })
   .done(function (data) {
       var announcements = data["value"];
       var theLength = announcements.length;
       var firstime = 'yes';
       $("#announce").append("<strong>Announcements</strong><BR><br>");
       $("#announce").append("No annoucements for " + $("#pagebuilder").val() + ' at this time');
       for (var i = theLength - 1; i >= 0; i--) {
           var proceed = 'no';
           if (eventtypes != 'allevents') {
               if (announcements[i].fields.Office_x002d_Dept_x002d_Team) {
                   for (var x = 0; x < announcements[i].fields.Office_x002d_Dept_x002d_Team.length; x++) {

                       if ($("#pagebuilder").val() == announcements[i].fields.Office_x002d_Dept_x002d_Team[x].LookupValue) proceed = 'yes';
                       if ($("#pagebuilder").val() == 'Home' && announcements[i].fields.EMA_x0020_Spotlight_x0020_Item == true) proceed = 'yes';
                   }
               }
           }
           else proceed = 'yes';
           if (proceed == 'yes') {
               if (firstime == 'yes') {
                   $("#announce").html('');
                   $("#announce").append("<BR><strong>Announcements</strong><BR><br>");
               }
               firstime = 'no';
               modalvaluearray[i] = announcements[i].fields.Body.replace('src="/', 'src="https://mower.sharepoint.com/').replace('href="/', 'target="_blank" href="https://mower.sharepoint.com/');
               modaltitle = 'Annoucement Detail';
               $("#announce").append('<a href="#" onClick="doannounce(modalvaluearray[' + i + '],modaltitle);">' + moment(announcements[i].createdDateTime).format("YYYY-MM-DD hh:mm A") + ' - ' + announcements[i].fields.Title + '</a> <br/> ');
           }
       }
   })
   .fail(function (jqXHR, textStatus) {
       var data = jqXHR.responseText;
       console.log('Error: Annoucements ' + data);
   });
}
//Get Birthdays and Anniversarys
function bdayaday() {
    if ($("#pagebuilder").val() == 'Home') {
        var jsonstring = "https://wv.mower.com/ws/service.svc/getpeople?callback=?";
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var todaysdate = new Date();
        var todaysdatestring = monthNames[todaysdate.getMonth()] + ' ' + todaysdate.getDate();
        var now = moment();
        var firstimea = 'yes';
        var firstimeb = 'yes';
        $.getJSON(jsonstring, function (data) {
            $.each(data, function (idx, obj) {
                $.each(obj, function (key, value) {
                    var theoffice = getoffice(value['OfficeId']);
                    var tablediv = '';
                    var holdann = moment(value['Anniversary'].toLowerCase());
                    if (value['BirthDay'].toLowerCase() == todaysdatestring.toLowerCase()) {
                        if (firstimeb == 'yes') $("#bday").append('<br><strong>Birthdays</strong><br><br>');
                        firstimeb = 'no';
                        tablediv = '<div class="people"><div style="display:inline-block;"><img src="https://wv.mower.com/ws/images/' + value['username'] + '600.jpg" onerror="imgError(this);"></div><div style="display:inline-block;"><a href="#" onClick="domodal(&quot;' + value['username'] + '&quot;);">' + value['WholeName'] + '</a><br>Supervisor: ' + value['Supervisor'] + '<br/>Office:' + theoffice + '<br/>Title: ' + value['Title'] + '</div></div>';
                        $("#bday").append(tablediv + '<br>');
                    }
                    if (holdann.date() == now.date() && holdann.month() == now.month()) {
                        if (firstimea == 'yes') $("#bday").append('<br><strong>Anniversary</strong><br><br>');
                        firstimea = 'no';
                        tablediv = '<div class="people"><div style="display:inline-block;"><img src="https://wv.mower.com/ws/images/' + value['username'] + '600.jpg" onerror="imgError(this);"></div><div style="display:inline-block;"><a href="#" onClick="domodal(&quot;' + value['username'] + '&quot;);">' + value['WholeName'] + '</a><br>Supervisor: ' + value['Supervisor'] + '<br/>Office:' + theoffice + '<br/>Title: ' + value['Title'] + '</div></div>';
                        $("#bday").append(tablediv + '<br>');

                    }
                    if (now.day() == 5) {
                        var hold1stdate = moment(holdann);
                        var hold2nddate = moment(holdann);
                        hold1stdate.subtract(1, 'd');
                        hold2nddate.subtract(2, 'd')
                        //$("#row4").append(value['WholeName'] + ' ' + hold1stdate.date() + ' ' +hold2nddate.date() + ' ' + holdann.date() +  '<br>');
                        if ((hold1stdate.date() == now.date() || hold2nddate.date() == now.date()) && holdann.month() == now.month()) {
                            $("#row4").append(value['WholeName'] + '<br>');
                        }
                    }
                });

            });
        });
    }
}
//GROUPS I BELONG TO
function groupsibelongto() {
    if ($("#pagebuilder").val() == 'Home') {
        $("#groupsbelong").append("<br><strong>Groups I belong to</strong><br><br>");
        $.ajax({
            url: "https://graph.microsoft.com/v1.0/me/joinedGroups",
            headers: { "Authorization": "Bearer " + token2 }
        })
            .done(function (data4) {
                var groups = data4["value"];
                var groupsLength = groups.length;
                var tablestring = '<div class="row2"><div class="col-4">Name</div><div class="col-2">Send Email</div><div class="col-6">Links</div></div>';

                for (var i = 0; i < groupsLength; i++) {
                    if (typeof groups[i] != "undefined") {
                        tablestring += ('<div class="row2"><div class="col-4">' + groups[i].displayName + '</div>');
                        tablestring += ('<div class="col-2"><a href="mailto:' + groups[i].mail + '"> <img src="/ws/testoned/icons/envelope.svg" style=" display: inline-block;width: 40px;"></a></div>');
                        var hrefconv = '<a href="https://outlook.office.com/owa/?realm=mower.com&exsvurl=1&ll-cc=1033&modurl=0&path=/group/' + groups[i].mail + '/mail" target="_blank">';
                        var hreffles = '<a href="https://outlook.office.com/owa/?realm=mower.com&exsvurl=1&ll-cc=1033&modurl=0&path=/group/' + groups[i].mail + '/files" target="_blank">';
                        var hrefcal = '<a href="https://outlook.office.com/owa/?realm=mower.com&exsvurl=1&ll-cc=1033&modurl=0&path=/group/' + groups[i].mail + '/calendar" target="_blank">';
                        var hrefnote = '<a href="https://mower.sharepoint.com/sites/' + groups[i].mailNickname + '/SiteAssets/' + encodeURI(groups[i].displayName) + '%20Notebook" target="blank">';
                        tablestring += ('<div class="col-6">' + hrefconv + '<img src="/ws/testoned/icons/outlook.svg" style=" display: inline-block;width: 40px;"></a>' + hrefcal + '<img src="/ws/testoned/icons/calendar.svg" style=" display: inline-block;width: 40px;"></a>' + hreffles + '<img src="/ws/testoned/icons/cloud.svg" style=" display: inline-block;width: 40px;"></a>' + hrefnote + '<img src="/ws/testoned/icons/onenote.svg" style=" display: inline-block;width: 40px;"></a></div></div>');
                    }
                }
                //tablestring += ('</table>');
                $("#groupsbelong").append(tablestring);
            })
            .fail(function (jqXHR, textStatus) {
                //alert("error: " + textStatus);
            });
    }
}
//Calendar
function getcalendar() {
    var jsonstring1 = "https://wv.mower.com/ws/service.svc/getcal";
    var calstring = '';
    var z = 0;
    var firstime = 'yes';
    $.getJSON(jsonstring1, function (data2) {
        //Need to format and place in the calendar then do chace
        $.each(data2, function (idx2, obj2) {
            $.each(obj2, function (key2, value2) {
                z = z + 1;
                var offdeptteam = value2['OffDeptTeam'].toString().split(',');
                var proceed = 'no';

                for (var i = 0; i < offdeptteam.length; i++) {
                    if ($("#pagebuilder").val() == offdeptteam[i]) proceed = 'yes';
                }
                if (proceed == 'yes') {
                    if (firstime == 'no') { calstring += ','; }
                    firstime = 'no';
                    modalvalue2 = value2['Description'];
                    modaltitle2 = 'Calander Detail';
                    calstring += '{"id":' + z + ',"name":"' + value2['Title'] + '","startdate":"' + moment(value2['EventDate']).format("YYYY-MM-DD") + '","enddate":"' + moment(value2['EndDate']).format("YYYY-MM-DD") + '","starttime":"' + moment(value2['EventDate']).format("hh:mm") + '","endtime":"' + moment(value2['EndDate']).format("hh:mm") + '","color":"","url":""}'
                }
            });
            if (calstring != '') {
                calstring = '{ "monthly": [' + calstring + ']}';
                $('#cal').monthly({
                    mode: 'event',
                    maxWidth: '600px',
                    dataType: 'json',
                    events: JSON.parse(calstring)
                });
            }
        });
    });
}
//HTML Snippett
function gethtml() {
    $.ajax({
        url: "https://graph.microsoft.com/beta/sites/mower.sharepoint.com,dc7f3b7e-5123-47ba-bcf1-a961abc250e7,504469bd-9e8d-4ecf-9e6b-f4d6e9743557/lists/4C0457C5-0EF8-4B1D-A827-7D2EA7AC193B/items?expand=fields",
        headers: { "Authorization": "Bearer " + token2 },
        cache: false
    })
    .done(function (data) {
        var htmlsnip = data["value"];
        var theLength = htmlsnip.length;
        for (var i = 0; i < theLength; i++) {
            if ($("#pagebuilder").val() == htmlsnip[i].fields.Office_x002d_Dept_x002d_Team[0].LookupValue) {
                var htmlparsed = htmlsnip[i].fields.HTML_x0020_Content.replace('src="/', 'src="https://mower.sharepoint.com/')
                $("#htmlpage").append('<div >' + htmlparsed + '</div>');
                $("#holdhtmlpages").append('<div id="home"  style="position: absolute;;width:50%;display:none;">' + htmlparsed + '</div>');
                if ($("#pagebuilder").val() == 'Syracuse') $("#mapresults").append('<div >' + '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2915.754458003352!2d-76.15635078410865!3d43.04659749945978!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d9f3bf423ceaf7%3A0x7510ae9156dfdc47!2s211+W+Jefferson+St%2C+Syracuse%2C+NY+13202!5e0!3m2!1sen!2sus!4v1507122856928" width="500" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>');
                if ($("#pagebuilder").val() == 'Buffalo') $("#mapresults").append('<div >' + '<iframe src="https://www.google.com/maps/embed?pb=!1m19!1m12!1m3!1d93542.09644282739!2d-78.9438745033065!3d42.88992705339398!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m4!3e6!4m0!4m1!2s50+Fountain+Plaza%2C+Suite+1300%2C+Buffalo%2C+NY+14202!5e0!3m2!1sen!2sus!4v1507577163649" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>');
                if ($("#pagebuilder").val() == 'Rochester') $("#mapresults").append('<div >' + '<iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d2910.5305555548016!2d-77.61372888992264!3d43.15638623967242!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x89d6b455bd13839b%3A0x5e01f7c452d3c8d7!2s28+E+Main+St+%231960%2C+Rochester%2C+NY+14614!3m2!1d43.1564076!2d-77.6115382!5e0!3m2!1sen!2sus!4v1507577375323" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>');
                if ($("#pagebuilder").val() == 'Atlanta') $("#mapresults").append('<div >' + '<iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d106105.75681239381!2d-84.46470273245636!3d33.791536012674634!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x88f504583a80d521%3A0x5bf40b33d2d4c227!2s201+17th+St+NW+%23500%2C+Atlanta%2C+GA+30363!3m2!1d33.7915558!2d-84.39466259999999!5e0!3m2!1sen!2sus!4v1507577274985" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>');
                if ($("#pagebuilder").val() == 'Charlotte') $("#mapresults").append('<div>' + '<iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d1629.7335543778704!2d-80.85142099673791!3d35.21974043051572!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x8856a02a7367e66f%3A0x2080852c9d090c34!2s1001+Morehead+Square+Dr%2C+Charlotte%2C+NC+28203!3m2!1d35.2197606!2d-80.85032439999999!5e0!3m2!1sen!2sus!4v1507577333085" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>');
                if ($("#pagebuilder").val() == 'New York City') $("#mapresults").append('<div >' + '<iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d3022.1556963709154!2d-73.97628708459362!3d40.75860017932687!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x89c258fb80292437%3A0xc5365c8a770d93cf!2s40+E+52nd+St%2C+New+York%2C+NY+10022!3m2!1d40.7586002!2d-73.9740984!5e0!3m2!1sen!2sus!4v1507577234004" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>');
                if ($("#pagebuilder").val() == 'Cincinnati') $("#mapresults").append('<div >' + '<iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d24768.250753081586!2d-84.52796329847169!3d39.10575654674116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x8841b159ca774e07%3A0x8dac797f2a0ed295!2s830+Main+St%2C+Cincinnati%2C+OH+45202!3m2!1d39.105777499999995!2d-84.51045169999999!5e0!3m2!1sen!2sus!4v1507577460269" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>');
                if ($("#pagebuilder").val() == 'Albany') $("#mapresults").append('<div >' + '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2934.5908084915823!2d-73.7551440845345!3d42.64883397916863!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89de0a214547f413%3A0x1269ffbfd3c6e48!2s30+S+Pearl+St+%231210%2C+Albany%2C+NY+12207!5e0!3m2!1sen!2sus!4v1507576913901" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>');
                if ($("#pagebuilder").val() == 'Boston') $("#mapresults").append('<div>' + '<iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d5896.50155049611!2d-71.24563459909443!3d42.358494586273814!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x89e3831deaccfbc3%3A0x2def9bf7d2ef45f0!2s134+Rumford+Ave+%23307%2C+Auburndale%2C+MA+02466!3m2!1d42.3585159!2d-71.2412552!5e0!3m2!1sen!2sus!4v1507577420651" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>');
            }
        }
    })
    .fail(function (jqXHR, textStatus) {
        //alert("error: " + textStatus);
        var data = jqXHR.responseText;
        console.log('Error: HTML ' + data);
    });
}
//Get Pages
function getpages() {
    $("#holdhtmlpages").empty();
    $.ajax({
        url: "https://graph.microsoft.com/beta/sites/mower.sharepoint.com,dc7f3b7e-5123-47ba-bcf1-a961abc250e7,504469bd-9e8d-4ecf-9e6b-f4d6e9743557/lists/d42823e4-8266-4477-9b30-e5f914279cc0/items?expand=fields",
        headers: { "Authorization": "Bearer " + token2 },
        cache: false
    })
    .done(function (data) {
        var htmlsnip = data["value"];
        var theLength = htmlsnip.length;
        var firsttime = 'yes';
        if (theLength > 0) {
           
        }
        for (var i = 0; i < theLength; i++) {
            if (htmlsnip[i].fields.Office_x002d_Dept_x002d_Team !='') {
                if ($("#pagebuilder").val() == htmlsnip[i].fields.Office_x002d_Dept_x002d_Team[0].LookupValue) {
                    if (firsttime == 'yes') {
                       
                        $("#homepagemenu").append('<a href="#" onclick="swappages(\'home\');">Home</a> | ');
                        firsttime = 'no';
                    }
                    var pagename = htmlsnip[i].fields.LinkFilename.replace('.aspx', '').replace(/\s/g, '');
                    $("#homepagemenu").append('<a href="#" onclick="swappages(\'' + pagename + '\');">' + htmlsnip[i].fields.LinkFilename.replace('.aspx', '') + '</a> | ');
                    $("#holdhtmlpages").append('<div id="' + pagename + '" style="position: absolute;;width:50%;display:none;">' + htmlsnip[i].fields.WikiField.replace('src="/', 'src="https://mower.sharepoint.com/') + '</div>');
               
            }
        }
        }
       
    })
    .fail(function (jqXHR, textStatus) {
        //alert("error: " + textStatus);
        var data = jqXHR.responseText;
        console.log('Error: HTML ' + data);
    });
}
//Get Pages
function swappages(pagename) {
    //$("#" + pagename).show();
    //Need to hide the rest
    var holddiv = $('#htmlpage').find('div').first();
    $('#htmlpage').find('div').first().remove();
    $("#htmlpage").append($("#" + pagename).html());
}
//Get My Files
function getmyfiles() {
    $('#filesholder').show();
    treestring = '';

    $.ajax({
        url: "https://graph.microsoft.com/v1.0/me/drive/root/children",
        headers: { "Authorization": "Bearer " + token2 }
    })
    .done(function (data4) {
        var myfiles = data4["value"];
        //console.log(JSON.stringify(myfiles));
        var theLength = myfiles.length;
        for (var i = 0; i < theLength; i++) {
            var child = '';
            if (myfiles[i].folder && myfiles[i].folder.childCount > 0) {
                child = 't';
            }
            if (myfiles[i].folder) {
                
                treestring += '{ "id" : "' + myfiles[i].id + '", "text" : "<div style=display:inline-block;>' + myfiles[i].name + '</div><div style=float:right;>' + moment(myfiles[i].lastModifiedDateTime).format("YYYY-MM-DD hh:mm A") + '</div>", "children" : "' + child + '", "icon":"/ws/testoned/icons/afolder.svg"  },';
            }
            else {
                useicon = geticon(myfiles[i].name);
                treestring += '{ "id" : "' + myfiles[i].name + '", "text" : "<div style=display:inline-block;>' + myfiles[i].name + '</div><div style=float:right;>' + moment(myfiles[i].lastModifiedDateTime).format("YYYY-MM-DD hh:mm A") + '</div>", "children" : "' + child + '", "icon":"/ws/testoned/icons/' + useicon + '","a_attr" : {"href" : "' + myfiles[i].webUrl + '", "target" : "_blank"} },';
            }
        }
        if (treestring != '') $("#filesholder").show();
        treestring = '[' + treestring.toString().substring(0, treestring.toString().length - 1) + ']';
        $('#jstree_div').on('close_node.jstree', function (e, data) {
            allchildren = data.node.children.length;
            for (var x = 0; x < allchildren ; x++) {
                $("#jstree_div").jstree("delete_node", data.node.children[0]);
            }
            var newNode = { id: 't', text: 't' };
            $('#jstree_div').jstree('create_node', data.node.id, newNode)
           
        })
        $('#jstree_div').on('open_node.jstree', function (e, data) {
            $("#jstree_div").jstree("delete_node", data.node.children[0]);
            var myfilesa;
            var nodestring = '';
            $.ajax({
                url: 'https://graph.microsoft.com/beta/me/drive/items/' + data.node.id + '/children',
                headers: { "Authorization": "Bearer " + token2 }
            })
          .done(function (data4a) {
              myfilesa = data4a["value"];
              var theLengtha = myfilesa.length;
              for (var k = 0; k < theLengtha; k++) {
                  var thename = '<div style=display:inline-block;>' + myfilesa[k].name + '</div><div style=float:right;>' + moment(myfilesa[k].lastModifiedDateTime).format("YYYY-MM-DD hh:mm A") + '</div>';
                  if (myfilesa[k].folder && myfilesa[k].folder.childCount >= 0) {
                      useicon = '/ws/testoned/icons/afolder.svg';
                      
                      newNode = { id: myfilesa[k].id, text: thename, children: "t", icon:useicon };
                  }
                  else {
                      useicon = '/ws/testoned/icons/' + geticon(myfilesa[k].name);
                      newNode = { id: myfilesa[k].name,  text: thename, icon:  useicon };
                  }
                  $('#jstree_div').jstree('create_node', data.node.id, newNode)
              }
              return newNode;
          })
          .fail(function (jqXHR, textStatus) {
              //alert("error: " + textStatus);
              console.log('Error: Files ' + data4a)
          });
        })
        $(function () {
            //alert('function firing')
           
            $('#jstree_div').jstree({
                'core': { 'data': JSON.parse(treestring), check_callback: true }

            }).bind("select_node.jstree", function (e, data) {
                window.open(data.node.a_attr.href, data.node.a_attr.target);
            });
        });
    })

        .fail(function (jqXHR, textStatus) {
            //alert("error: " + textStatus);
        });
}


//Files Shared with me
function filessharedwithme()
{
    var treestring3 = '';
    $.ajax({
        url: "https://graph.microsoft.com/beta/me/drive/sharedWithMe",
        headers: { "Authorization": "Bearer " + token2 }
    })
    .done(function (data4) {
        var myfiles = data4["value"];
        var theLength = myfiles.length;
        for (var i = 0; i < theLength; i++) {
          
            if ((myfiles[i].folder) && myfiles[i].folder.childCount >= 0) {
                treestring3 += '{ "id" : "' + myfiles[i].remoteItem.id + ',' + myfiles[i].remoteItem.parentReference.driveId + '", "text" : " <div style=display:inline-block;>' + myfiles[i].name + '</div><div style=float:right;>' + moment(myfiles[i].lastModifiedDateTime).format("YYYY-MM-DD hh:mm A") + '</div>' +  '","children" : "t","icon":"/ws/testoned/icons/afolder.svg" },';
            }
            
            else {
                useicon = geticon(myfiles[i].name);
                treestring3 += '{ "id" : "' + myfiles[i].name + '",  "text" : " <div style=display:inline-block;>' + myfiles[i].name + '</div><div style=float:right;>' + moment(myfiles[i].lastModifiedDateTime).format("YYYY-MM-DD hh:mm A") + '</div>' + '", "icon":"/ws/testoned/icons/' + useicon + '" ,"a_attr" : {"href" : "' + myfiles[i].webUrl + '", "target" : "_blank"}},';
            }
        }  
        if (treestring3 != '') $("#filesholder").show();
        treestring3 = '[' + treestring3.toString().substring(0, treestring3.toString().length - 1) + ']';
        //Open Node
        //url: 'https://graph.microsoft.com/beta/me/drive/sharedWithMe/items/' + data.node.id + '/children',
        $('#jstree2_div').on('open_node.jstree', function (e, data) {
            $("#jstree2_div").jstree("delete_node", data.node.children[0]);
            var myfilesa;
            var nodestring = '';
            var remoteinfo = data.node.id.split(',');
            $.ajax({
                url: 'https://graph.microsoft.com/beta/drives/' + remoteinfo[1] + '/items/' + remoteinfo[0] + '/children',
                
                headers: { "Authorization": "Bearer " + token2 }
            })
          .done(function (data4b) {
             
              myfilesb = data4b["value"];
              var theLengthb = myfilesb.length;
              for (var k = 0; k < theLengthb; k++) {
                  if (myfilesb[k].folder && myfilesb[k].folder.childCount >= 0) {
                      useicon = '/ws/testoned/icons/afolder.svg';
                      var filesids = myfilesb[k].id + ',' + remoteinfo[1];
                      newNode = { id: filesids, text: '<div style=display:inline-block;>' + myfilesb[k].name + '</div><div style=float:right;>' + moment(myfilesb[k].lastModifiedDateTime).format("YYYY-MM-DD hh:mm A") + '</div>', children: "t", icon: useicon };
                  }
                  else {
                      useicon = '/ws/testoned/icons/' + geticon(myfilesb[k].name);
                      newNode = { id: myfilesb[k].name, text: '<div style=display:inline-block;>' + myfilesb[k].name + '</div><div style=float:right;>' + moment(myfilesb[k].lastModifiedDateTime).format("YYYY-MM-DD hh:mm A") + '</div>', icon: useicon };
                  }
                  $('#jstree2_div').jstree('create_node', data.node.id, newNode)
              }
              return newNode;
          })
          .fail(function (jqXHR, textStatus) {
              //alert("error: " + textStatus);
              console.log('Error: Files ' + data4b)
          });
        })
        //End Open Node
        $(function () {
            $('#jstree2_div').jstree({
                'plugins': ['sort'],
                    'sort': function (a, b) {
                        a1 = this.get_node(a);
                        b1 = this.get_node(b);
                        if (a1.icon == b1.icon) {
                            return (a1.text > b1.text) ? 1 : -1;
                        } else {
                            return (a1.icon > b1.icon) ? 1 : -1;
                        }
                    },
                'core': {
                     'error' : function (err) { console.log(err); }, 
                    'data': JSON.parse(treestring3),
                    
                    check_callback: true
                }
            }).bind("select_node.jstree", function (e, data) {
                window.open(data.node.a_attr.href, data.node.a_attr.target);
            });
        })
    })
    .fail(function (jqXHR, textStatus) {
        //alert("error: " + textStatus);
    });
}
//Agency/HR/IT/Acct Files
function getagencyfiles(agencyfilesurl, type) {
   
    //if (type != 'Information Technology' && type != 'Accounting' && type != 'Human Resources') type='other'//WORKING HERE
        //console.log('Start Caching Data for ' + type)
        $.ajax({
            url: agencyfilesurl,
            headers: { "Authorization": "Bearer " + token2 }
        })
                .done(function (data2) {
                    var files = data2["value"];
                    //if (type != 'Human Resources' && type != 'Accounting' && type != 'Information Technology') console.log (JSON.stringify(files))
                    
                    var theLength = files.length;
                    treestring2 = '';
                    var firstime = 'yes';
                    var holdpaths = [];
                    var temppath = '';
                    for (var i = 0; i < theLength; i++) {
                        var proceed = 'no';
                        if (files[i].fields.Document_x0020_Tag) {
                            for (var x = 0; x < files[i].fields.Document_x0020_Tag.length; x++) {
                                if ($("#pagebuilder").val() == files[i].fields.Document_x0020_Tag[x].LookupValue) proceed = 'yes';
                            }
                        }
                        if (files[i].fields.Document_x0020_Tag0) {
                            for (var x = 0; x < files[i].fields.Document_x0020_Tag0.length; x++) {
                                if ($("#pagebuilder").val() == files[i].fields.Document_x0020_Tag0[x].LookupValue) proceed = 'yes';
                            }
                        }
                        //if ($("#pagebuilder").val() == 'Information Technology' || $("#pagebuilder").val() == 'Accounting' || $("#pagebuilder").val() == 'Human Resources') proceed = 'yes';
                        if (proceed == 'yes') {
                            if (typeof files[i].fields != "undefined") {
                                //parse weburl to determine if folders exist if they do then build folder structure and place file in it. Save folder info
                                var theparent = type;
                                var filename = files[i].webUrl.substring(files[i].webUrl.lastIndexOf('/') + 1, files[i].webUrl.length);
                                var urlparts = files[i].webUrl.split('/');
                                var foundlibrary = 'no';
                                var howmanyfolders = 0;
                                var lastfolder = '';
                                var temptreestring = '';
                                if (firstime == 'yes') temptreestring = '{ "id" : "' + type + '", "parent" : "#", "text" : "' + type + '", "state" : {"closed" : "true"},"icon":"/ws/testoned/icons/afolder.svg"  },';
                                for (var y = 0; y < urlparts.length; y++) {
                                    //I've already found libary now need to create folders
                                    if (foundlibrary == 'yes') {
                                        if (urlparts[y] != filename) {
                                            //if it's the first one in the loop and it's the first time we found it
                                            if (howmanyfolders == 0) {
                                                temptreestring += '{ "id" : "' + urlparts[y] + '", "parent" : "' + type + '", "text" : "' + decodeURIComponent(urlparts[y]) + '","icon":"/ws/testoned/icons/afolder.svg"  },';
                                                theparent = urlparts[y];
                                                howmanyfolders = howmanyfolders + 1;
                                                lastfolder = urlparts[y];
                                                temppath += urlparts[y];
                                            }
                                            else {
                                                temptreestring += '{ "id" : "' + urlparts[y] + '", "parent" : "' + lastfolder + '", "text" : "' + decodeURIComponent(urlparts[y]) + '","icon":"/ws/testoned/icons/afolder.svg"  },';
                                                theparent = urlparts[y];
                                                howmanyfolders = howmanyfolders + 1;
                                                lastfolder = urlparts[y];
                                                temppath += urlparts[y];
                                            }
                                        }

                                    }
                                    for (var z = 0; z < holdpaths.length; z++) {
                                        if (temppath == holdpaths[z]) temptreestring = '';
                                    }
                                    if (urlparts[y] == encodeURI($("#pagebuilder").val())) foundlibrary = 'yes';
                                    if (urlparts[y] == encodeURI('IT Public Document Library')) foundlibrary = 'yes';
                                    if (urlparts[y] == encodeURI('HR Public Document Library')) foundlibrary = 'yes';
                                    if (urlparts[y] == encodeURI('Accounting Public Document Library')) foundlibrary = 'yes';

                                }
                                if (temppath != '') holdpaths.push(temppath);
                                temppath = '';
                                treestring2 += temptreestring;
                                //End of folder logic
                                if (firstime == 'yes') $("#row5aa").append("<BR><strong>" + $("#pagebuilder").val() + " Files</strong><BR><br>");
                                firstime = 'no';
                                useicon = geticon(files[i].fields.FileLeafRef);
                                var isurl = 'no';
                                if (files[i].fields.FileLeafRef.indexOf('.url') >= 0) isurl = 'yes';
                                if (files[i].fields._dlc_DocIdUrl && isurl == 'no') {
                                    treestring2 += '{ "id" : "' + files[i].fields.FileLeafRef + '", "parent" : "' + theparent + '", "text" : "' + decodeURIComponent(files[i].fields.FileLeafRef) + '", "icon":"/ws/testoned/icons/' + useicon + '","a_attr" : {"href" : "' + files[i].fields._dlc_DocIdUrl.Url + '", "target" : "_blank"} },';
                                }
                                else {
                                    if (isurl == 'no') {
                                        treestring2 += '{ "id" : "' + files[i].fields.FileLeafRef + '", "parent" : "' + theparent + '", "text" : "' + decodeURIComponent(files[i].fields.FileLeafRef) + '", "icon":"/ws/testoned/icons/' + useicon + '","a_attr" : {"href" : "' + files[i].webUrl + '", "target" : "_blank"} },'
                                    }
                                    else {
                                        if (files[i].fields.url) {
                                            if (files[i].fields.url.indexOf('youtube') >= 0) {
                                                //Logic to create a url that opens video in modal
                                            }
                                            else {
                                                treestring2 += '{ "id" : "' + files[i].fields.FileLeafRef + '", "parent" : "' + theparent + '", "text" : "' + files[i].fields.Title + '", "icon":"/ws/testoned/icons/' + useicon + '","a_attr" : {"href" : "' + files[i].fields.url + '", "target" : "_blank"} },'
                                            }
                                        }
                                        else {
                                            treestring2 += '{ "id" : "' + files[i].fields.FileLeafRef + '", "parent" : "' + theparent + '", "text" : "' + files[i].fields.Title + '", "icon":"/ws/testoned/icons/' + useicon + '","a_attr" : {"href" : "http://' + files[i].fields.LinkFilename.substring(0, files[i].fields.LinkFilename.length - 3) + '", "target" : "_blank"} },'
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (treestring2 != '') {
                        $('#groupfileholder').show();
                        $('#grouptitleholder').html('<strong>' + $("#pagebuilder").val() + ' Files' + '</strong>');;
                    }
                    var seperator = '';
                    if (cachedataholder != '') seperator = '|';
                    treestring2 = '[' + treestring2.toString().substring(0, treestring2.toString().length - 1) + ']';
                    $(function () {
                        if (treestring2 != '[]') {
                            if (type == 'Information Technology') {
                                cachedataholder += seperator + '[Information Technology]|' + treestring2;
                                $('#jstree3_div').jstree({
                                    'core': { 'data': JSON.parse(treestring2), check_callback: true }
                                }).bind("select_node.jstree", function (e, data) {
                                    window.open(data.node.a_attr.href, data.node.a_attr.target);
                                });
                                $("#jstree3_div").jstree("close_all", -1);
                            }
                            if (type == 'Accounting') {
                                cachedataholder += seperator + '[Accounting]|' + treestring2;
                                $('#jstree4_div').jstree({
                                    'core': { 'data': JSON.parse(treestring2), check_callback: true }
                                }).bind("select_node.jstree", function (e, data) {
                                    window.open(data.node.a_attr.href, data.node.a_attr.target);
                                });
                                $("#jstree4_div").jstree("close_all", -1);
                            }
                            if (type == 'Human Resources') {
                                cachedataholder += seperator + '[Human Resources]|' + treestring2;
                                $('#jstree5_div').jstree({
                                    'core': { 'data': JSON.parse(treestring2), check_callback: true }
                                }).bind("select_node.jstree", function (e, data) {
                                    window.open(data.node.a_attr.href, data.node.a_attr.target);
                                });
                                $("#jstree5_div").jstree("close_all", -1);
                            }
                            if (type != 'Human Resources' && type != 'Accounting' && type != 'Information Technology') {
                                cachedataholder += seperator + '[Agency]|' + treestring2;
                                $('#jstree6_div').jstree({
                                    'core': { 'data': JSON.parse(treestring2), check_callback: true }
                                }).bind("select_node.jstree", function (e, data) {
                                    window.open(data.node.a_attr.href, data.node.a_attr.target);
                                });
                                $("#jstree6_div").jstree("close_all", -1);
                            }
                        }
                        if (type != 'Human Resources' && type != 'Accounting' && type != 'Information Technology') {
                            var myObject = new Object();
                            if (type == 'Public Relations & Public Affairs') type = 'PublicRelationsandPublicAffairs'
                            myObject.CacheName = type.replace(/\s+/g, '');
                            myObject.CacheData = cachedataholder;
                            //console.log('Finish Caching Data for ' + type)
                            var mystring = JSON.stringify(myObject);
                            //Cache the data
                            $.ajax({
                                type: "POST",
                                url: "https://wv.mower.com/ws/service.svc/savefilecache/",
                                data: mystring,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                processData: true,
                                success: function (data, status, jqXHR) {
                                    //alert("success… " + data);
                                    //domodal2(uname);
                                },
                                error: function (xhr, textStatus, errorThrown) {
                                    alert("Error_ " + xhr.responseXML + textStatus, errorThrown);
                                }
                            });
                        }
                    });

                })
                .fail(function (jqXHR, textStatus) {
                    var data2 = jqXHR.responseText;
                    console.log(data2);
                });
   
}
function getpeople() {
    jQuery(document).ready(function ($) {

        //Get Offices first
        var jsonstring1 = "https://wv.mower.com/ws/service.svc/getoffices?callback=?";

        $.getJSON(jsonstring1, function (data2) {
            $.each(data2, function (idx2, obj2) {
                $.each(obj2, function (key2, value2) {
                    holdoffices.push(value2)
                });
            });
        });
        var jsonstring = "https://wv.mower.com/ws/service.svc/getpeople?callback=?";
        $('#thetitle').empty();
        var holdofficeid = 0;
        buildtable = '<table>';
        buildtable = '';
        dsbuildtable = '<table id="dstable" class="depttables"><tr><td colspan="3"><h3>Digital Strategy</h3></td></tr>';
        smbuildtable = '<table id="smtable" class="depttables"><tr><td colspan="3"><h3>Search Engine Marketing</h3></td></tr>';
        devbuildtable = '<table id="detable" class="depttables"><tr><td colspan="3"><h3>Developers</h3></td></tr>';
        pmbuildtable = '<table id="pmtable" class="depttables"><tr><td colspan="3"><h3>Project Managers</h3></td></tr>';
        prodbuildtable = '<table id="prtable" class="depttables"><tr><td colspan="3"><h3>Production</h3></td></tr>';
        mediabuildtable = '<table id="metable"class="depttables"><tr><td colspan="3"><h3>Media</h3></td></tr>';
        creativebuildtable = '<table id="crtable" class="depttables"><tr><td colspan="3"><h3>Creative</h3></td></tr>';
        leadbuildtable1 = '<table id="le1table" class="depttables">';
        leadbuildtable2 = '<table id="le2table" class="depttables">';
        genericbuildtable = '';
        genericbuilddiv = '';
        var dstablevis = '';
        var smtablevis = '';
        var detablevis = '';
        var pmtablevis = '';
        var prtablevis = '';
        var metablevis = '';
        var smetablevis = '';
        var crtablevis = '';
        var le1tablevis = '';
        var le2tablevis = '';
        var gentablevis = '';
        officetitle = 'not found';
        var dsteam = ["mowusu", "kbroadbent"];
        var smteam = ["lniehaus"];
        var devteam = ["kdolbear", "jkerschner", "llazzaro", "tgentile", "smeyer"];
        var pmteam = ["dcomerford", "kgray"];
        var prodteam = ["rrandazzo", "bmorgen"];
        var mediateam = ["rgarland"];
        var creativeteam = ["mwheeler", "swheeler", "phildebrandt", "bkaiser", "yjiang"];
        var genericteam = [];
        $.getJSON(jsonstring, function (data) {
            $.each(data, function (idx, obj) {
                $.each(obj, function (key, value) {
                    if ($('#skillselect').val() != '') {
                        if (value['SkillTags'].indexOf($('#skillselect :selected').text()) == -1) {
                            return;
                        }
                    }

                    var theoffice = getoffice(value['OfficeId']);
                    var tablestring = '<tr><td><img src="https://wv.mower.com/ws/images/' + value['username'] + '600.jpg" onerror="imgError(this);"></td><td valign="top"><a href="#" onClick="domodal(&quot;' + value['username'] + '&quot;);">' + value['WholeName'] + '</a><br>Supervisor: ' + value['Supervisor'] + '<br/>Office:' + theoffice + '<br/>Title: ' + value['Title'] + '</td></tr>';
                    var tablediv = '<div class="people"><div style="display: inline-block;"><img src="https://wv.mower.com/ws/images/' + value['username'] + '600.jpg" onerror="imgError(this);"></div><div style="display: inline-block;width:230px;vertical-align: top;margin-left:5px"><a href="#" onClick="domodal(&quot;' + value['username'] + '&quot;);">' + value['WholeName'] + '</a><br>Supervisor: ' + value['Supervisor'] + '<br/>Office:' + theoffice + '<br/>Title: ' + value['Title'] + '</div></div>';

                    if (value['username'].toLowerCase() == 'jhenderson') {
                        leadbuildtable1 += tablestring;
                        le1tablevis = 'yes';
                    }
                    if (value['username'].toLowerCase() == 'enaismith') {
                        leadbuildtable2 += tablestring;
                        le2tablevis = 'yes'
                    }
                    if (dsteam.indexOf(value['username'].toLowerCase()) != -1) {
                        dsbuildtable += tablestring;
                        dstablevis = 'yes';
                    }
                    if (smteam.indexOf(value['username'].toLowerCase()) != -1) {
                        smbuildtable += tablestring;
                        smtablevis = 'yes';
                    }
                    if (devteam.indexOf(value['username'].toLowerCase()) != -1) {
                        devbuildtable += tablestring;
                        detablevis = 'yes';
                    }
                    if (pmteam.indexOf(value['username'].toLowerCase()) != -1) {
                        pmbuildtable += tablestring;
                        pmtablevis = 'yes';
                    }
                    if (prodteam.indexOf(value['username'].toLowerCase()) != -1) {
                        prodbuildtable += tablestring;
                        prtablevis = 'yes';
                    }
                    if (mediateam.indexOf(value['username'].toLowerCase()) != -1) {
                        mediabuildtable += tablestring;
                        metablevis = 'yes';
                    }
                    if (creativeteam.indexOf(value['username'].toLowerCase()) != -1) {
                        creativebuildtable += tablestring;
                        crtablevis = 'yes';

                    }
                    if (genericteam.indexOf(value['username'].toLowerCase()) != -1) {
                        genericbuildtable += tablediv;
                        gentablevis = 'yes';

                    }
                    if (theoffice.toString().toLowerCase() == $("#pagebuilder").val().toString().toLowerCase()) {
                        genericbuildtable += tablediv;

                        gentablevis = 'yes';

                    }
                    if (value['Dept2'].toString().toLowerCase() == 'pr' && $("#pagebuilder").val().toString() == 'Public Relations & Public Affairs') {
                        genericbuildtable += tablediv;
                        gentablevis = 'yes';
                    }
                    if (value['Dept2'].toString().toLowerCase() == 'crea' && $("#pagebuilder").val().toString() == 'Creative') {
                        genericbuildtable += tablediv;
                        gentablevis = 'yes';
                    }
                    if (value['Dept2'].toString().toLowerCase() == 'prmg' && $("#pagebuilder").val().toString() == 'Project Management') {
                        genericbuildtable += tablediv;
                        gentablevis = 'yes';
                    }
                    if (value['Dept2'].toString().toLowerCase() == 'acsv' && $("#pagebuilder").val().toString() == 'Account Service') {
                        genericbuildtable += tablediv;
                        gentablevis = 'yes';
                    }
                    if (value['Dept2'].toString().toLowerCase() == 'acct' && $("#pagebuilder").val().toString() == 'Accounting') {
                        genericbuildtable += tablediv;
                        gentablevis = 'yes';
                    }
                    if (value['Dept2'].toString().toLowerCase() == 'prod' && $("#pagebuilder").val().toString() == 'Production') {
                        genericbuildtable += tablediv;
                        gentablevis = 'yes';
                    }
                    if (value['Dept2'].toString().toLowerCase() == 'med' && $("#pagebuilder").val().toString() == 'Media') {
                        genericbuildtable += tablediv;
                        gentablevis = 'yes';
                    }
                    if (value['Dept2'].toString().toLowerCase() == 'hr' && $("#pagebuilder").val().toString() == 'Human Resources') {
                        genericbuildtable += tablediv;
                        gentablevis = 'yes';
                    }
                    if (value['Dept2'].toString().toLowerCase() == 'res' && $("#pagebuilder").val().toString() == 'Insight') {
                        genericbuildtable += tablediv;
                        gentablevis = 'yes';
                    }
                    if (value['Dept2'].toString().toLowerCase() == 'newb' && $("#pagebuilder").val().toString() == 'New Business') {
                        genericbuildtable += tablediv;
                        gentablevis = 'yes';
                    }
                    if (value['Dept2'].toString().toLowerCase() == 'stud' && $("#pagebuilder").val().toString() == 'Studio') {
                        genericbuildtable += tablediv;
                        gentablevis = 'yes';
                    }
                    if (value['Dept2'].toString().toLowerCase() == 'admi' && $("#pagebuilder").val().toString() == 'Administrative') {
                        genericbuildtable += tablediv;
                        gentablevis = 'yes';
                    }


                });

            });

            dsbuildtable += '</table>';
            smbuildtable += '</table>';
            devbuildtable += '</table>';
            pmbuildtable += '</table>';
            prodbuildtable += '</table>';
            mediabuildtable += '</table>';
            creativebuildtable += '</table>';
            leadbuildtable1 += '</table>';
            leadbuildtable2 += '</table>';
            if ($("#pagebuilder").val() == 'Digital') {
                buildtable += '<tr><td valign="top">' + leadbuildtable1 + devbuildtable + pmbuildtable + prodbuildtable + '</td><td valign="top">' + leadbuildtable2 + dsbuildtable + smbuildtable + '</td></tr>' + '<tr><td colspan="2"><hr></td></tr><tr><td colspan="2" align="center"><h2>Other Key Digital Experts</h2></td></tr><tr><td valign="top">' + mediabuildtable + '</td><td>' + creativebuildtable + '</td></tr></table>';
            }
            else {
                //buildtable += '<tr><td valign="top">' + genericbuildtable + '</td></tr>' + '</table>';
                buildtable = genericbuildtable;
            }
            emptyit();
            $('#holddata').append(buildtable);

            if (le1tablevis == 'yes') { $('#le1table').show(); }
            if (le2tablevis == 'yes') { $('#le2table').show(); }
            if (dstablevis == 'yes') { $('#dstable').show(); }
            if (smtablevis == 'yes') { $('#smtable').show(); }
            if (detablevis == 'yes') { $('#detable').show(); }
            if (pmtablevis == 'yes') { $('#pmtable').show(); }
            if (prtablevis == 'yes') { $('#prtable').show(); }
            if (metablevis == 'yes') { $('#metable').show(); }
            if (smetablevis == 'yes') { $('#smetable').show(); }
            if (crtablevis == 'yes') { $('#crtable').show(); }
            if (gentablevis == 'yes') { $('#gentable').show(); $("#searchit").show(); }
        })
        .fail(function (jqXHR, textStatus, errorThrown) { alert('getJSON request failed! ' + errorThrown); })
    });
}
function dosearch() {
    clearscreen();
    if ($("#searchstring").val() == '') return;
    $("#searchresults").html('');
    var myObject = new Object();
    myObject.SearchString = $("#searchstring").val();
    myObject.HowMany = 70;
    myObject.StartFrom = 1;
    var mystring = JSON.stringify(myObject);
    $.ajax({
        type: "POST",
        url: "https://wv.mower.com/ws/service.svc/dosearch",
        data: mystring,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
        success: function (data, status, jqXHR) {
            console.log(data)
            var searchresults = data;
            var theLength = searchresults.length;
            if (theLength < 1) $("#searchresults").append("No results found");
            for (var i = 0; i < theLength; i++) {
                var hrefstring = 'href="#"';
                hrefstring = 'href="' + searchresults[i].FileName + '"  target="_blank"';
                if (searchresults[i].Type == 'Snippits') {
                    hrefstring = 'href="#" onClick="dodetail(' + searchresults[i].FileName.split("?ID=")[1] + ',\'Snippits\');"';
                }
                if (searchresults[i].Type == 'Calender') {
                    hrefstring = 'href="#" onClick="dodetail(' + searchresults[i].FileName.split("?ID=")[1] + ',\'Calender\');"';
                }
                if (searchresults[i].Type == 'Announcements') {
                    hrefstring = 'href="#" onClick="dodetail(' +searchresults[i].FileName.split("?ID=")[1] + ',\'Announcements\');"';
                }
                if (searchresults[i].Type == 'Doc Library') hrefstring = 'href="' + searchresults[i].FileName + '"  target="_blank"';
                $("#searchresults").append(i+1 + ' - ' +'<a ' + hrefstring + '>' + searchresults[i].Type + ' - ' + searchresults[i].Title + '</a><br>' + searchresults[i].Summary + '<br><br>');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("Error_ " + xhr.responseXML + textStatus, errorThrown);
        }
    });
}
function dodetail(id,type) {
    //Display Annoucement in modal window after search
    var url = '';
    if (type == 'Announcements') url = 'https://graph.microsoft.com/v1.0/sites/mower.sharepoint.com,dc7f3b7e-5123-47ba-bcf1-a961abc250e7,504469bd-9e8d-4ecf-9e6b-f4d6e9743557/lists/0ADBBD66-8339-4625-915C-CC27D81A4D91/items/' + id + '?expand=fields';
    if (type == 'Calender') url = 'https://graph.microsoft.com/v1.0/sites/mower.sharepoint.com,dc7f3b7e-5123-47ba-bcf1-a961abc250e7,504469bd-9e8d-4ecf-9e6b-f4d6e9743557/lists/BBFE934C-8DEA-40BA-9437-FB99E25B80A6/items/' + id + '?expand=fields';
    if (type == 'Snippits') url = 'https://graph.microsoft.com/v1.0/sites/mower.sharepoint.com,dc7f3b7e-5123-47ba-bcf1-a961abc250e7,504469bd-9e8d-4ecf-9e6b-f4d6e9743557/lists/4C0457C5-0EF8-4B1D-A827-7D2EA7AC193B/items/' + id + '?expand=fields';
    $.ajax({
        url: url ,
        headers: { "Authorization": "Bearer " + token2 },
        cache: false
    })
  .done(function (data) {
     
      if (type == 'Announcements') {
          var annoucementdetail = data.fields.Body.replace('src="/', 'src="https://mower.sharepoint.com/').replace('href="/', 'target="_blank" href="https://mower.sharepoint.com/');
          modaltitle = 'Annoucement Detail';
          doannounce(annoucementdetail, modaltitle);
      }
      if (type == 'Calender') {
          var caldetail = 'Start Date ' + moment(data.fields.EventDate).format("YYYY-MM-DD hh:mm A") + ' End Date ' + moment(data.fields.EndDate).format("YYYY-MM-DD hh:mm A") + '<br>' + data.fields.Title + '<br><br>' + data.fields.Description.replace('src="/', 'src="https://mower.sharepoint.com/').replace('href="/', 'target="_blank" href="https://mower.sharepoint.com/');
          modaltitle = 'Calendar Detail';
          doannounce(caldetail, modaltitle);
      }
      if (type == 'Snippits') {
          //Find the office dept team associated and start initpage()
          //alert(data.fields.Office_x002d_Dept_x002d_Team[0].LookupValue);
          $("#pagebuilder").val(data.fields.Office_x002d_Dept_x002d_Team[0].LookupValue);
          initpage();
      }
  })
  .fail(function (jqXHR, textStatus) {
      var data = jqXHR.responseText;
      console.log('Error: Annoucements ' + data);
  });
}
//Utility Functions
function buildselect() {
    jQuery(document).ready(function ($) {
        var jsonstring = "https://wv.mower.com/ws/service.svc/getskills?callback=?";
        var buildoptions = '<option value="">All Skills (Use dropdown to filter by skill)</option>';
        $.getJSON(jsonstring, function (data) {
            $.each(data, function (idx, obj) {
                $.each(obj, function (key, value) {
                    buildoptions += '<option value="' + value['ID'] + '">' + value['SkillName'] + '</option>';
                });
            });
            $("#skillselect").empty();
            $("#skillselect").append(buildoptions);
        })
        .fail(function (jqXHR, textStatus, errorThrown) { alert('getJSON request failed! ' + errorThrown); })
    });
}

function geticon(filename) {
    var useicon = 'file.svg';
    if (filename.indexOf('.pdf') >= 0) useicon = 'pdf.svg';
    if (filename.indexOf('.doc') >= 0) useicon = 'word.svg';
    if (filename.indexOf('.docx') >= 0) useicon = 'word.svg';
    if (filename.indexOf('.ppt') >= 0) useicon = 'powerpoint.svg';
    if (filename.indexOf('.pptx') >= 0) useicon = 'powerpoint.svg';
    if (filename.indexOf('.xls') >= 0) useicon = 'excel.svg';
    if (filename.indexOf('.slsx') >= 0) useicon = 'excel.svg';
    if (filename.indexOf('.zip') >= 0) useicon = 'zip.svg';
    if (filename.indexOf('.mp4') >= 0) useicon = 'mp4.svg';
    if (filename.indexOf('.txt') >= 0) useicon = 'text.svg';
    return useicon;
}
function getoffice(officeid) {
    var theoffice = 'No Office';
    if (officeid == '1') { theoffice = 'Albany'; }
    if (officeid == '2') { theoffice = 'Atlanta'; }
    if (officeid == '3') { theoffice = 'Buffalo'; }
    if (officeid == '4') { theoffice = 'Syracuse'; }
    if (officeid == '7') { theoffice = 'Charlotte'; }
    if (officeid == '6') { theoffice = 'Rochester'; }
    if (officeid == '10') { theoffice = 'Cincinnati'; }
    if (officeid == '11') { theoffice = 'New York City'; }
    if (officeid == '12') { theoffice = 'Boston'; }
    return theoffice;
}
function sortByProperty(objArray, prop, direction) {
    if (arguments.length < 2) throw new Error("ARRAY, AND OBJECT PROPERTY MINIMUM ARGUMENTS, OPTIONAL DIRECTION");
    if (!Array.isArray(objArray)) throw new Error("FIRST ARGUMENT NOT AN ARRAY");
    const clone = objArray.slice(0);
    const direct = arguments.length > 2 ? arguments[2] : 1; //Default to ascending
    const propPath = (prop.constructor === Array) ? prop : prop.split(".");
    clone.sort(function (a, b) {
        for (let p in propPath) {
            if (a[propPath[p]] && b[propPath[p]]) {
                a = a[propPath[p]];
                b = b[propPath[p]];
            }
        }
        // convert numeric strings to integers
        a = a.match(/^\d+$/) ? +a : a;
        b = b.match(/^\d+$/) ? +b : b;
        return ((a < b) ? -1 * direct : ((a > b) ? 1 * direct : 0));
    });
    return clone;
}