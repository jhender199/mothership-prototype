﻿
	
function check_date(field){
    var checkstr = "0123456789";
    var DateField = field;
    var Datevalue = "";
    var DateTemp = "";
    var seperator = "/";
    var day;
    var month;
    var year;
    var leap = 0;
    var err = 0;
    var i;
    err = 0;
    DateValue = DateField.value;
    /* Delete all chars except 0..9 */
    for (i = 0; i < DateValue.length; i++) {
        if (checkstr.indexOf(DateValue.substr(i,1)) >= 0) {
            DateTemp = DateTemp + DateValue.substr(i,1);
        }
    }
    DateValue = DateTemp;
    /* Always change date to 8 digits - string*/
    /* if year is entered as 2-digit / always assume 20xx */
    if (DateValue.length == 6) {
        DateValue = DateValue.substr(0,4) + '20' + DateValue.substr(4,2); }
    if (DateValue.length != 8) {
        err = 19;}
    /* year is wrong if year = 0000 */
    year = DateValue.substr(4,4);
    if (year == 0) {
        err = 20;
    }
    /* Validation of month*/
    month = DateValue.substr(0,2);
    if ((month < 1) || (month > 12)) {
        err = 21;
    }
    /* Validation of day*/
    day = DateValue.substr(2,2);
    if (day < 1) {
        err = 22;
    }
    /* Validation leap-year / february / day */
    if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0)) {
        leap = 1;
    }
    if ((month == 2) && (leap == 1) && (day > 29)) {
        err = 23;
    }
    if ((month == 2) && (leap != 1) && (day > 28)) {
        err = 24;
    }
    /* Validation of other months */
    if ((day > 31) && ((month == "01") || (month == "03") || (month == "05") || (month == "07") || (month == "08") || (month == "10") || (month == "12"))) {
        err = 25;
    }
    if ((day > 30) && ((month == "04") || (month == "06") || (month == "09") || (month == "11"))) {
        err = 26;
    }
    /* if 00 ist entered, no error, deleting the entry */
    if ((day == 0) && (month == 0) && (year == 00)) {
        err = 0; day = ""; month = ""; year = ""; seperator = "";
    }
    /* if no error, write the completed date to Input-Field (e.g. 13.12.2001) */
    if (err == 0) {
        DateField.value =  month + seperator + day + seperator + year;
    }
        /* Error-message if err != 0 */
    else {
        alert("Date is incorrect!");
        DateField.select();
        DateField.focus();
    }
}


function openWin(holdcontent,whichtype)
{
    myWindow=window.open('','','width=1000,height=1000');
    if(whichtype=='pb')
    {
        myWindow.document.write('<style>@media print {	.page-break	{ display: block; page-break-before: always; }}</style><input type="button" value="Print This Page" onClick="window.print();"><br>' + holdcontent );
    }
    else
    {
        myWindow.document.write( '<input type="button" value="Print This Page" onClick="window.print();"><br>' + holdcontent );
    }
    myWindow.document.close(); //missing code
    myWindow.focus();
    //myWindow.print(); 
}

function laborreport() {
    buildcontent = '';
    $('#holddatalaborblock').empty();
    $("#laborblock").show();
    if ($("#office").val()=='' || $("#dept").val()=='' || $("#stdate").val()=='' || $("#endate").val()=='')
    {
        $("#holddatalaborblock").append("<font color=red>Missing Office Department Start Date or End Date</font>");
        return
    }
    var myObject = new Object();
    myObject.office= $('#office').val();
    myObject.department= $('#dept').val();
    myObject.startdate= $('#stdate').val();
    myObject.enddate= $('#endate').val();
    if ($("#detail").is(":checked"))
    {
        myObject.detail= $('#detail').val();
        detailtext='<td  valign="bottom" align="center" class="smaller"><strong>NB</strong></td><td  valign="bottom" align="center" class="smaller"><strong>Agbill</strong></td>';
        detailtext+= '<td  valign="bottom" align="center" class="smaller"><strong>Agency</strong></td><td  valign="bottom" align="center" class="smaller"><strong>Non-Bill Client</strong></td>'
    }
    else
    {
        myObject.detail= 'no';
        detailtext='<td  valign="bottom" align="center" class="smaller"><strong>Non-Job Related</strong></td>';
    }
    myObject.excel= $('#excel').val();
    myObject.subtotals= $('#subtotals').val();
    var mystring =JSON.stringify(myObject) ;
    console.log(mystring);
    holddept='';
	
    buildtable ='<tr><td valign="bottom" class="smaller"><strong>Employee</strong></td><td  valign="bottom" align="center" class="smaller"><strong>Status</strong></td><td  valign="bottom" align="center" class="smaller"><strong>Total Hours</strong></td><td  valign="bottom" align="center" class="smaller"><strong>Client Bill</strong></td><td  valign="bottom" align="center" class="smaller"><strong>%Target</strong><br><font size="1">(Client Bill)</font></td><td align="center" class="smaller"><strong>Client Labor</strong><br><font size="1">Bill&NonBill Client<br>Time,Agbill Time</font></td><td  valign="bottom" align="center" class="smaller"><strong>%Taget</strong><br><font size="1">(Client Labor)</font></td>' + detailtext + '</tr>';
    $.ajax({         
        type: "POST",
        url:"https://wv.mower.com/ws/service.svc/getlabor/",
        data: mystring ,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
        success: function (data, status, jqXHR) {
            tothours=0;
            clbill=0;
            cllabor=0;
            otherhours=0;
            stothours=0;
            sclbill=0;
            scllabor=0;
            sotherhours=0;
            agency=0;
            sagency=0;
            agbill=0;
            sagbill=0;
            newbiz=0;
            snewbiz=0;
            nonbill=0;
            snonbill=0;
            availtest=0.00;
            $.each(data, function(key, value){ 
                if ($("#partners").is(":checked") && value['PARTNER']!='True')
                {
                     	
                    return true;
                }
                     
                if (parseFloat(value['TOTAVAIL']).toFixed(2)>parseFloat(availtest))
                {
                     
                    $("#totdays").text(parseFloat(value['TOTAVAIL']/8).toFixed(2));
                    $("#8hourday").text(parseFloat(value['TOTAVAIL']).toFixed(2));
                    $("#9hourday").text(parseFloat(value['TOTAVAIL']/8*9).toFixed(2));
                    $("#tothours").text(parseFloat(value['TOTGOAL']).toFixed(2));
                    availtest=parseFloat(value['TOTAVAIL']).toFixed(2);
                }
                $("#results2").show()
                excelstring='<br><font size="1">goal:' + value['EMP_TOTALGOAL'] + '</font>';
                     
                if ($("#detail").is(":checked"))
                {
                    detailstring = '<td align="center">' + value['EMP_NEWBIZ'] + '</td><td align="center">' + value['EMP_AGBILL'] + '</td><td align="center">' + value['EMP_AGENCY'] + '</td><td align="center">' + value['EMP_NONBILL'] + '</td>';
                }
                else
                {
                    detailstring='<td align="center">' + value['EMP_NONJOBRELATED'] +'</td>'
                }
                     
                if ($("#excel").is(":checked"))
                {
                    excelstring ='';	
                 	
                }
                if(value['DP_TM_CODE']==holddept)
                {
                    buildcontent+= '<tr><td>' + value['EMP_ANN'] + value['EMP_NAME'] +'(' + value['EMP_CODE'] + ')</td><td align="center">' + value['EMP_STATUS'] +'</td><td align="center">' + parseFloat(value['BHOURS1'] + value['NBHOURS1']).toFixed(2) + '</td><td align="center">'+ parseFloat(value['HOURS2']).toFixed(2)+ '</td><td align="center">' + value['EMP_CLTARGET'] +'%' + excelstring + '</td><td align="center">' + parseFloat(value['HOURS1']).toFixed(2) + '</td><td align="center">' + value['EMP_CBTARGET'] +'%' + excelstring + '</td>' + detailstring +'</tr>'; 
                }
                else
                {
                    if(holddept =='')
                    {buildcontent+='<div class="page-break"></div><table>';}
                    else
                    {
                        if ($("#subtotals").is(":checked")  )
                        {
                            if ( $("#detail").is(":checked"))
                            {
                                buildcontent+='<tr><td>Subtotal</td><td>&nbsp;</td><td align="center">' + stothours.toFixed(2) + '</td><td align="center">' + sclbill.toFixed(2) + '</td><td>&nbsp;</td><td align="center">' + scllabor.toFixed(2) + '</td><td>&nbsp;</td><td align="center">' + snewbiz.toFixed(2) + '</td><td align="center">' + sagbill.toFixed(2)+ '</td><td align="center">' + sagency.toFixed(2) + '</td><td align="center">' + snonbill.toFixed(2)+ '</td></tr>';	
                            }
                            else
                            {
                                buildcontent+='<tr><td>Subtotal</td><td>&nbsp;</td><td align="center">' + stothours.toFixed(2) + '</td><td align="center">' + sclbill.toFixed(2) + '</td><td>&nbsp;</td><td align="center">' + scllabor.toFixed(2) + '</td><td>&nbsp;</td><td align="center">' + sotherhours.toFixed(2) + '</td></tr>';	
                            }
                        }
                        $("#results2").show();
                        if ($("#presults").is(":checked"))
                        {

                            buildcontent+='<tr><td colspan="10">' + $("#results2").html() + '</td></tr>'; 
                        }
                        buildcontent+='</table><div class="page-break"></div><table>';
                     		
                        stothours=0;
                        sclbill=0;
                        scllabor=0;
                        sotherhours=0;
                        sagency=0;
                        sagbill=0;
                        snewbiz=0;
                        snonbill=0;
                    }

                    buildcontent+='<tr><td colspan=10><h2>' + value['DP_TM_DESC'] + '</h2></td></tr>';
                    buildcontent+=buildtable;
                    buildcontent+='<tr><td>' + value['EMP_ANN'] + value['EMP_NAME'] +'(' + value['EMP_CODE'] + ')</td><td align="center">' + value['EMP_STATUS'] +'</td><td align="center">' + parseFloat(value['BHOURS1'] + value['NBHOURS1']).toFixed(2) + '</td><td align="center">'+ parseFloat(value['HOURS2']).toFixed(2)+ '</td><td align="center">' + value['EMP_CLTARGET'] +'%' + excelstring + '</td><td align="center">' + parseFloat(value['HOURS1']).toFixed(2) + '</td><td align="center">' + value['EMP_CBTARGET'] +'%' + excelstring + '</td>' + detailstring +'</tr>'; 
                    holddept = value['DP_TM_CODE'];
                     	
                }
                tothours=tothours + parseFloat(value['BHOURS1']) +  parseFloat(value['NBHOURS1']);
                clbill=clbill + parseFloat(value['HOURS2']);
                cllabor=cllabor+ parseFloat(value['HOURS1']);
                if (value['EMP_NONJOBRELATED']!='')
                {
                    otherhours =otherhours + parseFloat(value['EMP_NONJOBRELATED']);
                    sotherhours =sotherhours + parseFloat(value['EMP_NONJOBRELATED']);
                }
                agency=agency+parseFloat(value['EMP_AGENCY']);
                agbill=agbill+parseFloat(value['EMP_AGBILL']);
                sagbill=sagbill+parseFloat(value['EMP_AGBILL']);
                newbiz=newbiz+parseFloat(value['EMP_NEWBIZ']);
                snewbiz=snewbiz+parseFloat(value['EMP_NEWBIZ']);
                nonbill=nonbill+parseFloat(value['EMP_NONBILL']);
                snonbill=snonbill+parseFloat(value['EMP_NONBILL']);
                sagency=sagency+parseFloat(value['EMP_AGENCY']);
                stothours=stothours + parseFloat(value['BHOURS1']) + parseFloat(value['NBHOURS1']);
                sclbill=sclbill + parseFloat(value['HOURS2']);
                scllabor=scllabor+ parseFloat(value['HOURS1']);
                     
        				
                holddept=value['DP_TM_CODE'];
        				
            });
            if ($("#subtotals").is(":checked"))
            {
                if ( $("#detail").is(":checked"))
                {
                    buildcontent+='<tr><td>Subtotal</td><td>&nbsp;</td><td align="center">' + stothours.toFixed(2) + '</td><td align="center">' + sclbill.toFixed(2) + '</td><td>&nbsp;</td><td align="center">' + scllabor.toFixed(2) + '</td><td>&nbsp;</td><td align="center">' + snewbiz.toFixed(2) + '</td><td align="center">' + sagbill.toFixed(2)+ '</td><td align="center">' + sagency.toFixed(2) + '</td><td align="center">' + snonbill.toFixed(2)+ '</td></tr>';	
                    buildcontent+='<tr><td>Total</td><td>&nbsp;</td><td align="center">' + tothours.toFixed(2) + '</td><td align="center">' + clbill.toFixed(2) + '</td><td>&nbsp;</td><td align="center">' + cllabor.toFixed(2) + '</td><td>&nbsp;</td><td align="center">' + newbiz.toFixed(2) + '</td><td align="center">' + agbill.toFixed(2)+ '</td><td align="center">' + agency.toFixed(2) + '</td><td align="center">' + nonbill.toFixed(2)+ '</td></tr>';

                }
                else
                {
                    buildcontent+='<tr><td>Subtotal</td><td>&nbsp;</td><td align="center">' + stothours.toFixed(2) + '</td><td align="center">' + sclbill.toFixed(2) + '</td><td>&nbsp;</td><td align="center">' + scllabor.toFixed(2) + '</td><td>&nbsp;</td><td align="center">' + sotherhours.toFixed(2) + '</td></tr>';	
                    buildcontent+='<tr><td>Total</td><td>&nbsp;</td><td align="center">' + tothours.toFixed(2) + '</td><td align="center">' + clbill.toFixed(2) + '</td><td>&nbsp;</td><td align="center">' + cllabor.toFixed(2) + '</td><td>&nbsp;</td><td align="center">' + otherhours.toFixed(2) + '</td></tr>';
                }
            }
            if ($("#presults").is(":checked"))
            {
                buildcontent+='<tr><td colspan="10">' + $("#results2").html() + '</td></tr>';
            }
            buildcontent+='</table>';
            printcontent = '<h1>Labor Report</h1>' +  $("#office option:selected").text() + ' Office ' + $("#dept option:selected").text() ;
            if ($("#presults").is(":checked"))
            {
                printcontent += '<br> Starting Date: ' + $("#stdate").val()+ ' Ending Date: ' + $("#endate").val() +'<br><br>'+ buildcontent;
            }
            if (!$("#presults").is(":checked"))
            {
                printcontent += '<br> Starting Date: ' + $("#stdate").val()+ ' Ending Date: ' + $("#endate").val() +'<br><br>'+ $("#results2").html()+'<br><br>' + buildcontent;
            }
        				
            $("#holddatalaborblock").append('<input value="Open Print Window(no page breaks)" type="button" onclick="openWin(printcontent,\'npb\');" /><input value="Open Print Window(page breaks)" type="button" onclick="openWin(printcontent,\'pb\');" />'  );
            $("#holddatalaborblock").append(buildcontent);
        				
            $("#holddatalaborblock").append('<input value="Open Print Window(no page breaks)" type="button" onclick="openWin(printcontent,\'npb\');" /><input value="Open Print Window(page breaks)" type="button" onclick="openWin(printcontent,\'pb\');" />'  );
        				
        },
        error: function (xhr,textStatus, errorThrown) {
            alert("Error_ " + xhr.responseXML + textStatus,errorThrown);
        }  
    });
}

