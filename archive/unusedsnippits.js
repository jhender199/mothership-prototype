﻿if (idClicked == 'GetExcel') {
    $.ajax({
        url: "https://graph.microsoft.com/v1.0/me/drive/root:/Copy of gas_AMI_bus_case_WEB_101316_v3.xlsx:/workbook/worksheets('Entry')/range(address='G5')",
        headers: { "Authorization": "Bearer " + token2 }
    })
    .done(function (data4) {
        $("#holdmessages").append(JSON.stringify(data4));
        //  $("#holdmessages").append(JSON.stringify(data4));
        arr_from_json = JSON.parse(JSON.stringify(data4));
        $("#NPV").val(arr_from_json["values"][0]);

    })
    .fail(function (jqXHR, textStatus) {
        alert("error: " + textStatus);
    });

}
if (idClicked == 'PutExcel') {
    var myObject = new Object();
    myObject.values = $("#CustNum").val();;
    var mystring = JSON.stringify(myObject);

    $.ajax({
        dataType: "json",
        type: "PATCH",
        data: mystring,
        url: "https://graph.microsoft.com/v1.0/me/drive/root:/Copy of gas_AMI_bus_case_WEB_101316_v3.xlsx:/workbook/worksheets('Entry')/range(address='B5')",
        headers: { "Authorization": "Bearer " + token2 }
    })
    .done(function (data4) {
        $("#holdmessages").append(JSON.stringify(data4));
        //  $("#holdmessages").append(JSON.stringify(data4));
        arr_from_json = JSON.parse(JSON.stringify(data4));
        //$("#holdmessages").append('The id ' + arr_from_json["value"][0].columnSet.HTML_x0020_Content);

    })
    .fail(function (jqXHR, textStatus) {
        alert("error: " + textStatus);
    });

}
if (idClicked == 'GetExcelChart') {
    $.ajax({
        url: "https://graph.microsoft.com/v1.0/me/drive/root:/Copy of gas_AMI_bus_case_WEB_101316_v4.xlsx:/workbook/worksheets('Cust_Cash_Flow')/range(address='A1:X1')",
        headers: { "Authorization": "Bearer " + token2 }
    })
    .done(function (data4) {
        $("#holdmessages").append(JSON.stringify(data4));
        //  $("#holdmessages").append(JSON.stringify(data4));
        arr_from_json = JSON.parse(JSON.stringify(data4));
        //$("#NPV").val(arr_from_json["values"][0]);

    })
    .fail(function (jqXHR, textStatus) {
        alert("error: " + textStatus);
    });

}

//Old Search Engine
//Calendar (NOT COMPLETED OR TESTED

/*
$.ajax({
    url: "https://graph.microsoft.com/v1.0/sites/mower.sharepoint.com,dc7f3b7e-5123-47ba-bcf1-a961abc250e7,504469bd-9e8d-4ecf-9e6b-f4d6e9743557/lists/BBFE934C-8DEA-40BA-9437-FB99E25B80A6/items?expand=fields",
    headers: { "Authorization": "Bearer " + token2 },
    cache: false
})
   .done(function (data) {
       var calander = data["value"];
       var theLength = calander.length;
       var firstime = 'yes';
       for (var i = theLength - 1; i >= 0; i--) {
           if ((calander[i].fields.Title.indexOf($("#searchstring").val()) > -1 ) && $("#searchstring").val() != '') {
               if (firstime == 'yes') $("#searchresults").append("<BR><strong>Calendar</strong><BR><br>");
               firstime = 'no';
               modalvaluearray[i] = calander[i].fields.Body;
               modaltitle = 'Calendar Detail';
               $("#searchresults").append('<a href="#" onClick="doannounce(modalvaluearray[' + i + '],modaltitle);">' + moment(calander[i].fields.EventDate).format("hh:mm") + ' - ' + calander[i].fields.Title + '</a> <br/> ');
           }
       }
   })
   .fail(function (jqXHR, textStatus) {
       alert("error: " + textStatus);
       var data = jqXHR.responseText;
       console.log(data);
   });
//Had to settle for fake search with calendar and annoucement lists
//Announcements
$.ajax({
    url: "https://graph.microsoft.com/v1.0/sites/mower.sharepoint.com,dc7f3b7e-5123-47ba-bcf1-a961abc250e7,504469bd-9e8d-4ecf-9e6b-f4d6e9743557/lists/0ADBBD66-8339-4625-915C-CC27D81A4D91/items/?expand=fields",
    headers: { "Authorization": "Bearer " + token2 },
    cache: false
})
   .done(function (data) {
       var announcements = data["value"];
       var theLength = announcements.length;
       var firstime = 'yes';
       for (var i = theLength - 1; i >= 0; i--) {
           if ((announcements[i].fields.Title.indexOf($("#searchstring").val()) > -1 || announcements[i].fields.Body.indexOf($("#searchstring").val()) > -1) && $("#searchstring").val() !='') {
               if (firstime == 'yes') $("#searchresults").append("<BR><strong>Announcements</strong><BR><br>");
               firstime = 'no';
               modalvaluearray[i] = announcements[i].fields.Body;
               modaltitle = 'Annoucement Detail';
               $("#searchresults").append('<a href="#" onClick="doannounce(modalvaluearray[' + i + '],modaltitle);">' + moment(announcements[i].createdDateTime).format("YYYY-MM-DD hh:mm A") + ' - ' + announcements[i].fields.Title + '</a> <br/> ');
           }
       }
   })
   .fail(function (jqXHR, textStatus) {
       alert("error: " + textStatus);
       var data = jqXHR.responseText;
       console.log(data);
   });

//SP Docs Agency Files working
var spurls = [];
//agency
spurls[0] = 'https://graph.microsoft.com/v1.0/sites/mower.sharepoint.com,7ff72d6f-ed85-4f94-b92b-a34e78322b7a,a43fa114-7179-417a-8a62-6362813d0447/lists/D9B30730-109F-49F1-99BB-BFE50705843B/drive/root/search(q=\'' + $("#searchstring").val() + '\')';
//Run IT
spurls[1] ='https://graph.microsoft.com/beta/sites/mower.sharepoint.com,7ff72d6f-ed85-4f94-b92b-a34e78322b7a,a43fa114-7179-417a-8a62-6362813d0447/lists/160AB46E-5A38-4AFD-9FFD-328E1AF2CAC0/drive/root/search(q=\'' + $("#searchstring").val() + '\')';
//Run Accounting
spurls[2] ='https://graph.microsoft.com/beta/sites/mower.sharepoint.com,7ff72d6f-ed85-4f94-b92b-a34e78322b7a,a43fa114-7179-417a-8a62-6362813d0447/lists/97525365-4430-451F-90CD-4D631A00870A//drive/root/search(q=\'' + $("#searchstring").val() + '\')';
//Run HR
spurls[3] ='https://graph.microsoft.com/beta/sites/mower.sharepoint.com,7ff72d6f-ed85-4f94-b92b-a34e78322b7a,a43fa114-7179-417a-8a62-6362813d0447/lists/C9AEDDF9-A02C-4CE3-B6AE-997D69AAA820/drive/root/search(q=\'' + $("#searchstring").val() + '\')';
for (var i = 0; i < 4; i++) {
    $.ajax({
        url: spurls[i] 
        ,
        headers: { "Authorization": "Bearer " + token2 }
    })
        .done(function (data4) {
            var files = data4["value"];
            var theLength = files.length;
            if (theLength > 0) {
                $("#searchresults").append("<br /><br /><strong>Agency Files</strong><br /><br />");
                for (var i = 0; i < theLength; i++) {
                    $("#searchresults").append('<a href="' + files[i].webUrl + '" target="_blank">' + files[i].name + '</a><br/>');
                }
            }
        })
        .fail(function (jqXHR, textStatus) {
            alert("error: " + textStatus);
        });
}
//My Docs seems to be working
//Need to make this optional

$.ajax({
    url: "https://graph.microsoft.com/v1.0/me/drive/root/search(q='" + $("#searchstring").val() + "')",
    headers: { "Authorization": "Bearer " + token2 }
})
    .done(function (data4) {
        
        $("#searchresults").append("<br /><br />");
        var mydocs = data4["value"];
        var theLength = mydocs.length;
        if (theLength > 0) {
            $("#searchresults").append("<br /><br /><strong>My Docs</strong> <br /><br />");
            for (var i = 0; i < theLength; i++) {
                if (mydocs[i].folder) $("#searchresults").append('Folder -' + mydocs[i].webUrl.replace('https://mower-my.sharepoint.com', ''))
                $("#searchresults").append('<a href="' + mydocs[i].webUrl + '" target="_blank">' + mydocs[i].name + '</a><br/>');
            }
            
        }
    })
    .fail(function (jqXHR, textStatus) {
        alert("error: " + textStatus);
    });  */