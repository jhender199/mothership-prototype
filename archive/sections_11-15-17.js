﻿//Get My Files
function getmyfiles() {
    $('#filesholder').show();
    treestring = '';

    $.ajax({
        url: "https://graph.microsoft.com/v1.0/me/drive/root/children",
        headers: { "Authorization": "Bearer " + token2 }
    })
    .done(function (data4) {
        var myfiles = data4["value"];
        //console.log(JSON.stringify(myfiles));
        var theLength = myfiles.length;
        for (var i = 0; i < theLength; i++) {
            var child = '';
            if (myfiles[i].folder && myfiles[i].folder.childCount > 0) {
                child = 't';
            }
            if (myfiles[i].folder) {
                
                treestring += '{ "id" : "' + myfiles[i].id + '", "text" : "<div style=display:inline-block;>' + myfiles[i].name + '</div><div style=float:right;>' + moment(myfiles[i].lastModifiedDateTime).format("YYYY-MM-DD hh:mm A") + '</div>", "children" : "' + child + '", "icon":"/ws/testoned/icons/afolder.svg"  },';
            }
            else {
                useicon = geticon(myfiles[i].name);
                treestring += '{ "id" : "' + myfiles[i].name + '", "text" : "<div style=display:inline-block;>' + myfiles[i].name + '</div><div style=float:right;>' + moment(myfiles[i].lastModifiedDateTime).format("YYYY-MM-DD hh:mm A") + '</div>", "children" : "' + child + '", "icon":"/ws/testoned/icons/' + useicon + '","a_attr" : {"href" : "' + myfiles[i].webUrl + '", "target" : "_blank"} },';
            }
        }
        if (treestring != '') $("#filesholder").show();
        treestring = '[' + treestring.toString().substring(0, treestring.toString().length - 1) + ']';
        $('#jstree_div').on('close_node.jstree', function (e, data) {
            allchildren = data.node.children.length;
            for (var x = 0; x < allchildren ; x++) {
                $("#jstree_div").jstree("delete_node", data.node.children[0]);
            }
            var newNode = { id: 't', text: 't' };
            $('#jstree_div').jstree('create_node', data.node.id, newNode)
           
        })
        $('#jstree_div').on('open_node.jstree', function (e, data) {
            $("#jstree_div").jstree("delete_node", data.node.children[0]);
            var myfilesa;
            var nodestring = '';
            $.ajax({
                url: 'https://graph.microsoft.com/beta/me/drive/items/' + data.node.id + '/children',
                headers: { "Authorization": "Bearer " + token2 }
            })
          .done(function (data4a) {
              myfilesa = data4a["value"];
              var theLengtha = myfilesa.length;
              for (var k = 0; k < theLengtha; k++) {
                  var thename = '<div style=display:inline-block;>' + myfilesa[k].name + '</div><div style=float:right;>' + moment(myfilesa[k].lastModifiedDateTime).format("YYYY-MM-DD hh:mm A") + '</div>';
                  if (myfilesa[k].folder && myfilesa[k].folder.childCount >= 0) {
                      useicon = '/ws/testoned/icons/afolder.svg';
                      
                      newNode = { id: myfilesa[k].id, text: thename, children: "t", icon:useicon };
                  }
                  else {
                      useicon = '/ws/testoned/icons/' + geticon(myfilesa[k].name);
                      newNode = { id: myfilesa[k].name,  text: thename, icon:  useicon };
                  }
                  $('#jstree_div').jstree('create_node', data.node.id, newNode)
              }
              return newNode;
          })
          .fail(function (jqXHR, textStatus) {
              //alert("error: " + textStatus);
              console.log('Error: Files ' + data4a)
          });
        })
        $(function () {
            //alert('function firing')
           
            $('#jstree_div').jstree({
                'core': { 'data': JSON.parse(treestring), check_callback: true }

            }).bind("select_node.jstree", function (e, data) {
                window.open(data.node.a_attr.href, data.node.a_attr.target);
            });
        });
    })

        .fail(function (jqXHR, textStatus) {
            //alert("error: " + textStatus);
        });
}


//Files Shared with me
function filessharedwithme()
{
    var treestring3 = '';
    $.ajax({
        url: "https://graph.microsoft.com/beta/me/drive/sharedWithMe",
        headers: { "Authorization": "Bearer " + token2 }
    })
    .done(function (data4) {
        var myfiles = data4["value"];
        var theLength = myfiles.length;
        for (var i = 0; i < theLength; i++) {
          
            if ((myfiles[i].folder) && myfiles[i].folder.childCount >= 0) {
                treestring3 += '{ "id" : "' + myfiles[i].remoteItem.id + ',' + myfiles[i].remoteItem.parentReference.driveId + '", "text" : "' + myfiles[i].name + '","children" : "t","icon":"/ws/testoned/icons/afolder.svg" },';
            }
            
            else {
                useicon = geticon(myfiles[i].name);
                treestring3 += '{ "id" : "' + myfiles[i].name + '",  "text" : "' + myfiles[i].name + '", "icon":"/ws/testoned/icons/' + useicon + '" ,"a_attr" : {"href" : "' + myfiles[i].webUrl + '", "target" : "_blank"}},';
            }
        }  
        if (treestring3 != '') $("#filesholder").show();
        treestring3 = '[' + treestring3.toString().substring(0, treestring3.toString().length - 1) + ']';
        //Open Node
        //url: 'https://graph.microsoft.com/beta/me/drive/sharedWithMe/items/' + data.node.id + '/children',
        $('#jstree2_div').on('open_node.jstree', function (e, data) {
            $("#jstree2_div").jstree("delete_node", data.node.children[0]);
            var myfilesa;
            var nodestring = '';
            var remoteinfo = data.node.id.split(',');
            $.ajax({
                url: 'https://graph.microsoft.com/beta/drives/' + remoteinfo[1] + '/items/' + remoteinfo[0] + '/children',
                
                headers: { "Authorization": "Bearer " + token2 }
            })
          .done(function (data4b) {
             
              myfilesb = data4b["value"];
              var theLengthb = myfilesb.length;
              for (var k = 0; k < theLengthb; k++) {
                  if (myfilesb[k].folder && myfilesb[k].folder.childCount >= 0) {
                      useicon = '/ws/testoned/icons/afolder.svg';
                      var filesids = myfilesb[k].id + ',' + remoteinfo[1];
                      newNode = { id: filesids, text: myfilesb[k].name, children: "t", icon: useicon };
                  }
                  else {
                      useicon = '/ws/testoned/icons/' + geticon(myfilesb[k].name);
                      newNode = { id: myfilesb[k].name, text: myfilesb[k].name, icon: useicon };
                  }
                  $('#jstree2_div').jstree('create_node', data.node.id, newNode)
              }
              return newNode;
          })
          .fail(function (jqXHR, textStatus) {
              //alert("error: " + textStatus);
              console.log('Error: Files ' + data4b)
          });
        })
        //End Open Node
        $(function () {
            $('#jstree2_div').jstree({
                'plugins': ['sort'],
                    'sort': function (a, b) {
                        a1 = this.get_node(a);
                        b1 = this.get_node(b);
                        if (a1.icon == b1.icon) {
                            return (a1.text > b1.text) ? 1 : -1;
                        } else {
                            return (a1.icon > b1.icon) ? 1 : -1;
                        }
                    },
                'core': {
                     'error' : function (err) { console.log(err); }, 
                    'data': JSON.parse(treestring3),
                    
                    check_callback: true
                }
            }).bind("select_node.jstree", function (e, data) {
                window.open(data.node.a_attr.href, data.node.a_attr.target);
            });
        })
    })
    .fail(function (jqXHR, textStatus) {
        //alert("error: " + textStatus);
    });
}
//Agency/HR/IT/Acct Files
function getagencyfiles(agencyfilesurl, type) {
   
    //if (type != 'Information Technology' && type != 'Accounting' && type != 'Human Resources') type='other'//WORKING HERE
        //console.log('Start Caching Data for ' + type)
        $.ajax({
            url: agencyfilesurl,
            headers: { "Authorization": "Bearer " + token2 }
        })
                .done(function (data2) {
                    var files = data2["value"];
                    //if (type != 'Human Resources' && type != 'Accounting' && type != 'Information Technology') console.log (JSON.stringify(files))
                    if (type == 'Public Relations & Public Affairs') console.log('PR FILES' + JSON.stringify(files))
                    var theLength = files.length;
                    treestring2 = '';
                    var firstime = 'yes';
                    var holdpaths = [];
                    var temppath = '';
                    for (var i = 0; i < theLength; i++) {
                        var proceed = 'no';
                        if (files[i].fields.Document_x0020_Tag) {
                            for (var x = 0; x < files[i].fields.Document_x0020_Tag.length; x++) {
                                if ($("#pagebuilder").val() == files[i].fields.Document_x0020_Tag[x].LookupValue) proceed = 'yes';
                            }
                        }
                        if (files[i].fields.Document_x0020_Tag0) {
                            for (var x = 0; x < files[i].fields.Document_x0020_Tag0.length; x++) {
                                if ($("#pagebuilder").val() == files[i].fields.Document_x0020_Tag0[x].LookupValue) proceed = 'yes';
                            }
                        }
                        //if ($("#pagebuilder").val() == 'Information Technology' || $("#pagebuilder").val() == 'Accounting' || $("#pagebuilder").val() == 'Human Resources') proceed = 'yes';
                        if (proceed == 'yes') {
                            if (typeof files[i].fields != "undefined") {
                                //parse weburl to determine if folders exist if they do then build folder structure and place file in it. Save folder info
                                var theparent = type;
                                var filename = files[i].webUrl.substring(files[i].webUrl.lastIndexOf('/') + 1, files[i].webUrl.length);
                                var urlparts = files[i].webUrl.split('/');
                                var foundlibrary = 'no';
                                var howmanyfolders = 0;
                                var lastfolder = '';
                                var temptreestring = '';
                                if (firstime == 'yes') temptreestring = '{ "id" : "' + type + '", "parent" : "#", "text" : "' + type + '", "state" : {"opened" : "true"},"icon":"/ws/testoned/icons/afolder.svg"  },';
                                for (var y = 0; y < urlparts.length; y++) {

                                    //I've already found libary now need to create folders
                                    if (foundlibrary == 'yes') {
                                        if (urlparts[y] != filename) {
                                            //if it's the first one in the loop and it's the first time we found it
                                            if (howmanyfolders == 0) {
                                                temptreestring += '{ "id" : "' + urlparts[y] + '", "parent" : "' + type + '", "text" : "' + decodeURIComponent(urlparts[y]) + '","icon":"/ws/testoned/icons/afolder.svg"  },';
                                                theparent = urlparts[y];
                                                howmanyfolders = howmanyfolders + 1;
                                                lastfolder = urlparts[y];
                                                temppath += urlparts[y];
                                            }
                                            else {
                                                temptreestring += '{ "id" : "' + urlparts[y] + '", "parent" : "' + lastfolder + '", "text" : "' + decodeURIComponent(urlparts[y]) + '","icon":"/ws/testoned/icons/afolder.svg"  },';
                                                theparent = urlparts[y];
                                                howmanyfolders = howmanyfolders + 1;
                                                lastfolder = urlparts[y];
                                                temppath += urlparts[y];
                                            }
                                        }

                                    }
                                    for (var z = 0; z < holdpaths.length; z++) {
                                        if (temppath == holdpaths[z]) temptreestring = '';
                                    }
                                    if (urlparts[y] == encodeURI($("#pagebuilder").val())) foundlibrary = 'yes';
                                    if (urlparts[y] == encodeURI('IT Public Document Library')) foundlibrary = 'yes';
                                    if (urlparts[y] == encodeURI('HR Public Document Library')) foundlibrary = 'yes';
                                    if (urlparts[y] == encodeURI('Accounting Public Document Library')) foundlibrary = 'yes';

                                }
                                if (temppath != '') holdpaths.push(temppath);
                                temppath = '';
                                treestring2 += temptreestring;
                                //End of folder logic
                                if (firstime == 'yes') $("#row5aa").append("<BR><strong>" + $("#pagebuilder").val() + " Files</strong><BR><br>");
                                firstime = 'no';
                                useicon = geticon(files[i].fields.FileLeafRef);
                                var isurl = 'no';
                                if (files[i].fields.FileLeafRef.indexOf('.url') >= 0) isurl = 'yes';
                                if (files[i].fields._dlc_DocIdUrl && isurl == 'no') {
                                    treestring2 += '{ "id" : "' + files[i].fields.FileLeafRef + '", "parent" : "' + theparent + '", "text" : "' + decodeURIComponent(files[i].fields.FileLeafRef) + '", "icon":"/ws/testoned/icons/' + useicon + '","a_attr" : {"href" : "' + files[i].fields._dlc_DocIdUrl.Url + '", "target" : "_blank"} },';
                                }
                                else {
                                    if (isurl == 'no') {
                                        treestring2 += '{ "id" : "' + files[i].fields.FileLeafRef + '", "parent" : "' + theparent + '", "text" : "' + decodeURIComponent(files[i].fields.FileLeafRef) + '", "icon":"/ws/testoned/icons/' + useicon + '","a_attr" : {"href" : "' + files[i].webUrl + '", "target" : "_blank"} },'
                                    }
                                    else {
                                        if (files[i].fields.url) {
                                            if (files[i].fields.url.indexOf('youtube') >= 0) {
                                                //Logic to create a url that opens video in modal
                                            }
                                            else {
                                                treestring2 += '{ "id" : "' + files[i].fields.FileLeafRef + '", "parent" : "' + theparent + '", "text" : "' + files[i].fields.Title + '", "icon":"/ws/testoned/icons/' + useicon + '","a_attr" : {"href" : "' + files[i].fields.url + '", "target" : "_blank"} },'
                                            }
                                        }
                                        else {
                                            treestring2 += '{ "id" : "' + files[i].fields.FileLeafRef + '", "parent" : "' + theparent + '", "text" : "' + files[i].fields.Title + '", "icon":"/ws/testoned/icons/' + useicon + '","a_attr" : {"href" : "http://' + files[i].fields.LinkFilename.substring(0, files[i].fields.LinkFilename.length - 3) + '", "target" : "_blank"} },'
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (treestring2 != '') {
                        $('#groupfileholder').show();
                        $('#grouptitleholder').html('<strong>' + $("#pagebuilder").val() + ' Files' + '</strong>');;
                    }
                    var seperator = '';
                    if (cachedataholder != '') seperator = '|';
                    treestring2 = '[' + treestring2.toString().substring(0, treestring2.toString().length - 1) + ']';
                    $(function () {
                        if (treestring2 != '[]') {
                            if (type == 'Information Technology') {
                                cachedataholder += seperator + '[Information Technology]|' + treestring2;
                                $('#jstree3_div').jstree({
                                    'core': { 'data': JSON.parse(treestring2), check_callback: true }
                                }).bind("select_node.jstree", function (e, data) {
                                    window.open(data.node.a_attr.href, data.node.a_attr.target);
                                });
                            }
                            if (type == 'Accounting') {
                                cachedataholder += seperator + '[Accounting]|' + treestring2;
                                $('#jstree4_div').jstree({
                                    'core': { 'data': JSON.parse(treestring2), check_callback: true }
                                }).bind("select_node.jstree", function (e, data) {
                                    window.open(data.node.a_attr.href, data.node.a_attr.target);
                                });
                            }
                            if (type == 'Human Resources') {
                                cachedataholder += seperator + '[Human Resources]|' + treestring2;
                                $('#jstree5_div').jstree({
                                    'core': { 'data': JSON.parse(treestring2), check_callback: true }
                                }).bind("select_node.jstree", function (e, data) {
                                    window.open(data.node.a_attr.href, data.node.a_attr.target);
                                });
                            }
                            if (type != 'Human Resources' && type != 'Accounting' && type != 'Information Technology') {
                                cachedataholder += seperator + '[Agency]|' + treestring2;
                                $('#jstree6_div').jstree({
                                    'core': { 'data': JSON.parse(treestring2), check_callback: true }
                                }).bind("select_node.jstree", function (e, data) {
                                    window.open(data.node.a_attr.href, data.node.a_attr.target);
                                });
                            }
                        }
                        if (type != 'Human Resources' && type != 'Accounting' && type != 'Information Technology') {
                            var myObject = new Object();
                            if (type == 'Public Relations & Public Affairs') type = 'PublicRelationsandPublicAffairs'
                            myObject.CacheName = type.replace(/\s+/g, '');
                            myObject.CacheData = cachedataholder;
                            //console.log('Finish Caching Data for ' + type)
                            var mystring = JSON.stringify(myObject);
                            //Cache the data
                            $.ajax({
                                type: "POST",
                                url: "https://wv.mower.com/ws/service.svc/savefilecache/",
                                data: mystring,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                processData: true,
                                success: function (data, status, jqXHR) {
                                    //alert("success… " + data);
                                    //domodal2(uname);
                                },
                                error: function (xhr, textStatus, errorThrown) {
                                    alert("Error_ " + xhr.responseXML + textStatus, errorThrown);
                                }
                            });
                        }
                    });

                })
                .fail(function (jqXHR, textStatus) {
                    var data2 = jqXHR.responseText;
                    console.log(data2);
                });
   
}
function dosearch() {
    alert($("#searchstring").val());
    if ($("#searchstring").val() == '') return;
    $("#row0").html('');
    //Calendar (NOT COMPLETED OR TESTED
    $.ajax({
        url: "https://graph.microsoft.com/v1.0/sites/mower.sharepoint.com,dc7f3b7e-5123-47ba-bcf1-a961abc250e7,504469bd-9e8d-4ecf-9e6b-f4d6e9743557/lists/BBFE934C-8DEA-40BA-9437-FB99E25B80A6/items?expand=fields",
        headers: { "Authorization": "Bearer " + token2 },
        cache: false
    })
       .done(function (data) {
           var calander = data["value"];
           var theLength = calander.length;
           var firstime = 'yes';
           for (var i = theLength - 1; i >= 0; i--) {
               if ((calander[i].fields.Title.indexOf($("#searchstring").val()) > -1 || calander[i].fields.Body.indexOf($("#searchstring").val()) > -1) && $("#searchstring").val() != '') {
                   if (firstime == 'yes') $("#row0").append("<BR><strong>Calendar</strong><BR><br>");
                   firstime = 'no';
                   modalvaluearray[i] = calander[i].fields.Body;
                   modaltitle = 'Calendar Detail';
                   $("#row0").append('<a href="#" onClick="doannounce(modalvaluearray[' + i + '],modaltitle);">' + moment(calander[i].fields.EventDate).format("hh:mm") + ' - ' + calander[i].fields.Title + '</a> <br/> ');
               }
           }
       })
       .fail(function (jqXHR, textStatus) {
           alert("error: " + textStatus);
           var data = jqXHR.responseText;
           console.log(data);
       });
    //Had to settle for fake search with calendar and annoucement lists
    //Announcements
    $.ajax({
        url: "https://graph.microsoft.com/v1.0/sites/mower.sharepoint.com,dc7f3b7e-5123-47ba-bcf1-a961abc250e7,504469bd-9e8d-4ecf-9e6b-f4d6e9743557/lists/0ADBBD66-8339-4625-915C-CC27D81A4D91/items/?expand=fields",
        headers: { "Authorization": "Bearer " + token2 },
        cache: false
    })
       .done(function (data) {
           var announcements = data["value"];
           var theLength = announcements.length;
           var firstime = 'yes';
           for (var i = theLength - 1; i >= 0; i--) {
               if ((announcements[i].fields.Title.indexOf($("#searchstring").val()) > -1 || announcements[i].fields.Body.indexOf($("#searchstring").val()) > -1) && $("#searchstring").val() !='') {
                   if (firstime == 'yes') $("#row0").append("<BR><strong>Announcements</strong><BR><br>");
                   firstime = 'no';
                   modalvaluearray[i] = announcements[i].fields.Body;
                   modaltitle = 'Annoucement Detail';
                   $("#row0").append('<a href="#" onClick="doannounce(modalvaluearray[' + i + '],modaltitle);">' + moment(announcements[i].createdDateTime).format("YYYY-MM-DD hh:mm A") + ' - ' + announcements[i].fields.Title + '</a> <br/> ');
               }
           }
       })
       .fail(function (jqXHR, textStatus) {
           alert("error: " + textStatus);
           var data = jqXHR.responseText;
           console.log(data);
       });
    
    //SP Docs Agency Files working

    $.ajax({
        url: "https://graph.microsoft.com/beta/sites/mower.sharepoint.com,7ff72d6f-ed85-4f94-b92b-a34e78322b7a,a43fa114-7179-417a-8a62-6362813d0447/lists/C9AEDDF9-A02C-4CE3-B6AE-997D69AAA820/drive/root/search(q='" + $("#searchstring").val() + "')"
        ,
        headers: { "Authorization": "Bearer " + token2 }
        })
        .done(function (data4) {
            var files = data4["value"];
            var theLength = files.length;
            if (theLength > 0) {
                $("#row0").append("<br /><br /><strong>Agency Files</strong><br /><br />");
                for (var i = 0; i < theLength; i++) {
                    $("#row0").append('<a href="' + files[i].webUrl + '" target="_blank">' + files[i].name + '</a><br/>');
                }
               
            }
        })
        .fail(function (jqXHR, textStatus) {
            alert("error: " + textStatus);
        });
    //My Docs seems to be working
    $.ajax({
        url: "https://graph.microsoft.com/v1.0/me/drive/root/search(q='" + $("#searchstring").val() + "')",
        headers: { "Authorization": "Bearer " + token2 }
    })
        .done(function (data4) {
            
            $("#row0").append("<br /><br />");
            var mydocs = data4["value"];
            var theLength = mydocs.length;
            if (theLength > 0) {
                $("#row0").append("<br /><br /><strong>My Docs</strong> <br /><br />");
                for (var i = 0; i < theLength; i++) {
                    if (mydocs[i].folder) $("#row0").append('Folder -' + mydocs[i].webUrl.replace('https://mower-my.sharepoint.com',''))
                    $("#row0").append('<a href="' + mydocs[i].webUrl + '" target="_blank">' + mydocs[i].name + '</a><br/>');
                }
                
            }
        })
        .fail(function (jqXHR, textStatus) {
            alert("error: " + textStatus);
        });          
}
//Utility Functions

function geticon(filename) {
    var useicon = 'file.svg';
    if (filename.indexOf('.pdf') >= 0) useicon = 'pdf.svg';
    if (filename.indexOf('.doc') >= 0) useicon = 'word.svg';
    if (filename.indexOf('.docx') >= 0) useicon = 'word.svg';
    if (filename.indexOf('.ppt') >= 0) useicon = 'powerpoint.svg';
    if (filename.indexOf('.pptx') >= 0) useicon = 'powerpoint.svg';
    if (filename.indexOf('.xls') >= 0) useicon = 'excel.svg';
    if (filename.indexOf('.slsx') >= 0) useicon = 'excel.svg';
    if (filename.indexOf('.zip') >= 0) useicon = 'zip.svg';
    if (filename.indexOf('.mp4') >= 0) useicon = 'mp4.svg';
    if (filename.indexOf('.txt') >= 0) useicon = 'text.svg';
    return useicon;
}
function getoffice(officeid) {
    var theoffice = 'No Office';
    if (officeid == '1') { theoffice = 'Albany'; }
    if (officeid == '2') { theoffice = 'Atlanta'; }
    if (officeid == '3') { theoffice = 'Buffalo'; }
    if (officeid == '4') { theoffice = 'Syracuse'; }
    if (officeid == '7') { theoffice = 'Charlotte'; }
    if (officeid == '6') { theoffice = 'Rochester'; }
    if (officeid == '10') { theoffice = 'Cincinnati'; }
    if (officeid == '11') { theoffice = 'New York City'; }
    if (officeid == '12') { theoffice = 'Boston'; }
    return theoffice;
}
function sortByProperty(objArray, prop, direction) {
    if (arguments.length < 2) throw new Error("ARRAY, AND OBJECT PROPERTY MINIMUM ARGUMENTS, OPTIONAL DIRECTION");
    if (!Array.isArray(objArray)) throw new Error("FIRST ARGUMENT NOT AN ARRAY");
    const clone = objArray.slice(0);
    const direct = arguments.length > 2 ? arguments[2] : 1; //Default to ascending
    const propPath = (prop.constructor === Array) ? prop : prop.split(".");
    clone.sort(function (a, b) {
        for (let p in propPath) {
            if (a[propPath[p]] && b[propPath[p]]) {
                a = a[propPath[p]];
                b = b[propPath[p]];
            }
        }
        // convert numeric strings to integers
        a = a.match(/^\d+$/) ? +a : a;
        b = b.match(/^\d+$/) ? +b : b;
        return ((a < b) ? -1 * direct : ((a > b) ? 1 * direct : 0));
    });
    return clone;
}