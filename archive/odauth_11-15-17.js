var idClicked = '';
function odauth(wasClicked) {
   $("#holdmessages").html('');
   //$("#holdmessages").append('<br>Starting odauth function ' + idClicked + ' was clicked <br>');
  ensureHttps();
  var token = getTokenFromCookie();
  if (token) {
      //   $("#holdmessages").append('<br>odauth function found token<br>');
      $("#thebuttons").show();

    onAuthenticated(token);
  }
  else if (wasClicked) {
   //   $("#holdmessages").append('<br>odauth function no token go to auth screen<br>');
    challengeForAuth();
  }
  else {
   //   $("#holdmessages").append('<br>odauth function not clicked no token<br>');
      //challengeForAuth();
      showLoginButton();
  }
}

// for added security we require https
function ensureHttps() {
  if (window.location.protocol != "https:") {
    window.location.href = "https:" + window.location.href.substring(window.location.protocol.length);
  }
}

function onAuthCallback() {
 //   $("#holdmessages").append('<br>Starting onAuthCallback function<br>');
  var authInfo = getAuthInfoFromUrl();
  //var token = authInfo["access_token"];
  var token = authInfo["id_token"];
//  $("#holdmessages").append('<br>onAuthCallback function' + token + '<br>');
  var expiry = parseInt(authInfo["expires_in"]);
  setCookie(token, expiry);
  window.opener.onAuthenticated(token, window);
}

function getAuthInfoFromUrl() {
    if (window.location.hash) {
        //alert(window.location.hash)
    var authResponse = window.location.hash.substring(1);
    var authInfo = JSON.parse(
      '{"' + authResponse.replace(/&/g, '","').replace(/=/g, '":"') + '"}',
      function(key, value) { return key === "" ? value : decodeURIComponent(value); });
    return authInfo;
  }
  else {
      $("#holdmessages").append('<br>Function getAuthInfoFrom Url in odauth.js no token recieved<br>');
  }
}

function getTokenFromCookie() {
  var cookies = document.cookie;
  var name = "odauth=";
  var start = cookies.indexOf(name);
  if (start >= 0) {
    start += name.length;
    var end = cookies.indexOf(';', start);
    if (end < 0) {
      end = cookies.length;
    }
    else {
      postCookie = cookies.substring(end);
    }

    var value = cookies.substring(start, end);
    return value;
  }

  return "";
}

function setCookie(token, expiresInSeconds) {
  var expiration = new Date();
  expiration.setTime(expiration.getTime() + expiresInSeconds * 1000);
  var cookie = "odauth=" + token +"; path=/; expires=" + expiration.toUTCString();

  if (document.location.protocol.toLowerCase() == "https") {
    cookie = cookie + ";secure";
  }

  document.cookie = cookie;
}
function setCookie2(token, expiresInSeconds) {
    var expiration = new Date();
    //Add back in *1000 for correct cookie lengh
    expiration.setTime(expiration.getTime() + expiresInSeconds * 1000);
    console.log('Cookie expires on ' + expiration.toString());
    var cookie = "appauth=" + token + "; path=/; expires=" + expiration.toUTCString();

    if (document.location.protocol.toLowerCase() == "https") {
        cookie = cookie + ";secure";
    }

    document.cookie = cookie;
}
function getTokenFromCookie2() {
    var cookies = document.cookie;
    var name = "appauth=";
    var start = cookies.indexOf(name);
    if (start >= 0) {
        start += name.length;
        var end = cookies.indexOf(';', start);
        if (end < 0) {
            end = cookies.length;
        }
        else {
            postCookie = cookies.substring(end);
        }

        var value = cookies.substring(start, end);
        return value;
    }

    return "";
}

function getAppInfo() {
  var scriptTag = document.getElementById("odauth");
  if (!scriptTag) {
    alert("the script tag for odauth.js should have its id set to 'odauth'");
  }

  var clientId = scriptTag.getAttribute("clientId");
  if (!clientId) {
    alert("the odauth script tag needs a clientId attribute set to your application id");
  }

  var scopes = scriptTag.getAttribute("scopes");
  if (!scopes) {
    alert("the odauth script tag needs a scopes attribute set to the scopes your app needs");
  }

  var redirectUri = scriptTag.getAttribute("redirectUri");
  if (!redirectUri) {
    alert("the odauth script tag needs a redirectUri attribute set to your redirect landing url");
  }

  var appInfo = {
    "clientId": clientId,
    "scopes": scopes,
    "redirectUri": redirectUri
  };

  return appInfo;
}

// called when a login button needs to be displayed for the user to click on.
// if a customLoginButton() function is defined by your app, it will be called
// with 'true' passed in to indicate the button should be added. otherwise, it
// will insert a textual login link at the top of the page. if defined, your
// showCustomLoginButton should call challengeForAuth() when clicked.
function showLoginButton() {
  if (typeof showCustomLoginButton === "function") {
    showCustomLoginButton(true);
    return;
  }

  var loginText = document.createElement('a');
  loginText.href = "#";
  loginText.id = "loginText";
  loginText.onclick = challengeForAuth;
  loginText.innerText = "[sign in]";
  document.body.insertBefore(loginText, document.body.children[0]);
}

// called with the login button created by showLoginButton() needs to be
// removed. if a customLoginButton() function is defined by your app, it will
// be called with 'false' passed in to indicate the button should be removed.
// otherwise it will remove the textual link that showLoginButton() created.
function removeLoginButton() {
  if (typeof showCustomLoginButton === "function") {
    showCustomLoginButton(false);
    return;
  }

  var loginText = document.getElementById("loginText");
  if (loginText) {
    document.body.removeChild(loginText);
  }
}
//+https%3A%2F%2Fgraph.microsoft.com%2FGroup.Read.All
function challengeForAuth() {
  var appInfo = getAppInfo();
  var url =
    "https://login.microsoftonline.com/common/oauth2/v2.0/authorize" +
    "?client_id=" + appInfo.clientId +
    "&scope=openid+profile+https%3A%2F%2Fgraph.microsoft.com%2FMail.Read+https%3A%2F%2Fgraph.microsoft.com%2FFiles.ReadWrite" +
    "+https%3A%2F%2Fgraph.microsoft.com%2FSites.Read.All+https%3A%2F%2Fgraph.microsoft.com%2FUser.Read" +
    "&response_type=id_token" +
    "&redirect_uri=" + encodeURIComponent(appInfo.redirectUri) +
      "&nonce=98797987978&promt=consent";
  popup(url);
 
}
function popup(url) {
  var width = 525,
      height = 525,
      screenX = window.screenX,
      screenY = window.screenY,
      outerWidth = window.outerWidth,
      outerHeight = window.outerHeight;

  var left = screenX + Math.max(outerWidth - width, 0) / 2;
  var top = screenY + Math.max(outerHeight - height, 0) / 2;
  var features = [
              "width=" + width,
              "height=" + height,
              "top=" + top,
              "left=" + left,
              "status=no",
              "resizable=yes",
              "toolbar=no",
              "menubar=no",
              "scrollbars=yes"];
  var popup = window.open(url, "oauth", features.join(","));
  if (!popup) {
    alert("failed to pop up auth window");
  }
  popup.focus();
}
